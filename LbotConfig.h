/*
 * LbotConfig.h
 *
 *  Created on: Mar 8, 2018
 *      Author: dkunin
 */

#ifndef LBOTCONFIG_H_
#define LBOTCONFIG_H_

#include "types.h"
#include "LbotEeprom.h"
#include <ESP8266WiFi.h>
#include "debugMacros.h"

#define HOST_SIZE	32
#define HOST_USER	16
#define HOST_PASS	16
#define SSID_SIZE	32
#define PASSW_SIZE	32
#define NAME_SIZE	16
#define TOPIC_SIZE  50

typedef struct {
	char ssid[SSID_SIZE];
	char passw[PASSW_SIZE];
	char ssid1[SSID_SIZE];
	char passw1[PASSW_SIZE];
	char ssid2[SSID_SIZE];
	char passw2[PASSW_SIZE];
	int data_server_port;
	int softAp;
	unsigned int ip;
	unsigned int gate;
	unsigned int mask;
	byte static_ip_enable;
	//// Update these with values suitable for your network.
	// IPAddress server(192, 168, 1, 1);
	//// Update these with values suitable for your network.
	// IPAddress server(192, 168, 1, 1);

	//// Update these with values suitable for your network.
	// IPAddress server(192, 168, 1, 1);
	char host_name[NAME_SIZE];
	char data_server_host[HOST_SIZE];
	char data_server_username[HOST_USER];
	char data_server_pass[HOST_PASS];
	char topic_path[TOPIC_SIZE];

	byte reserved[50];
	unsigned short CRC;

} config_image_t;


class LbotConfig {
public:
	LbotConfig();
	void begin(void);
	void set_default(void);

	void setSsid(char *ssid, int len );
	void setSsid(char *ssid);
	void setPassw(char *passw, int len );
	void setPassw(char *passw);
    void setHostName(char *hostName, int len );
    void setHostName(char *hostName);
    void setDataServerHost(char *dataServerHost, int len );
    void setDataServerUsername(char *dataServerUsername, int len );
    void setDataServerPass(char *dataServerPass, int len );
    void setTopicPath(char *topicPath, int len );
    void setDataServerPort(int data_server_port);
	void setSoftAp(boolean softAp);
	void setStaticIpEnable(byte staticIpEnable);
	void setIp(char *ip);
	void setGate(char *gate);
	void setMask(char *mask);
	void setIp(unsigned int ip);
	void setGate(unsigned int gate);
	void setMask(unsigned int mask);
	void setManualNetworkConf(byte manualNetworkConf);
	void save(void);
	void read(void);

	const unsigned int getIp(){
//		DPRINT("GetIP ");
//		DPRINTLN(m_config_image.ip);

		return m_config_image.ip;
	}

	const unsigned int getMask(){
		return m_config_image.mask;
	}

	const unsigned int getGate(){
		return m_config_image.gate;
	}

	const char* getHostName(){
		return m_config_image.host_name;
	}

	const char* getDataServerHost(){
		return m_config_image.data_server_host;
	}

	const char* getDataServerUsername(){
		return m_config_image.data_server_username;
	}

	const char* getDataServerPass(){
		return m_config_image.data_server_pass;
	}

	const int getDataServerPort(){
		return m_config_image.data_server_port;
	}

	const char* getTopicPath(){
		return m_config_image.topic_path;
	}

	const char* getPassw(){
		return m_config_image.passw;
	}

	const char* getSsid() const {
		return m_config_image.ssid;
	}

	const boolean isSoftAP(){
		if(m_config_image.softAp != 0) {
			return true;
		}
		return false;
	}

	boolean isStaticIpEnable(){
		if(m_config_image.static_ip_enable == 0) {
			return false;
		}
		return true;
	}

private:
	LbotEeprom eeprom;
	config_image_t m_config_image;

};

#endif /* LBOTCONFIG_H_ */
