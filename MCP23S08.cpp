#define MCP23S08_PUB
#include "Arduino.h"
#include "spidev.h"
#include "MCP23S08.h"

/*****************************************************************
 Function Name:    SPIWriteByte                                
 Return Value:                                             
 Parameters:       register address, and data.               
 Description:      This routine performs a byte write.         
 *******************************************************************/
void MCP23S08Write(unsigned char reg, unsigned char data)
{

	selectSpiDev (PORT_CS);  // Enable SPI Communication to  MCP23S08
	transferSpiDev(CONTROL_BYTE | WR_CMD | ADDR_PINS, 0x10);
	transferSpiDev(reg, 0x10);
	transferSpiDev(data, 0x10);
	selectSpiDev (NONE_CS);  // Disable SPI Communication to  MCP23S08
}

/*****************************************************************
 Function Name:    SPIReadByte                                
 Return Value:     Data at register                                        
 Parameters:       Register
 Description:      This routine performs a sequential write.         
 *******************************************************************/
unsigned char MCP23S08Read(unsigned char reg)
{
	unsigned char n;

	selectSpiDev (PORT_CS);  // Enable SPI Communication to  MCP23S08
	transferSpiDev(CONTROL_BYTE | RD_CMD | ADDR_PINS, 0x10);
	transferSpiDev(reg, 0x10);
	n = transferSpiDev(0, 0x10);
	selectSpiDev (NONE_CS);  // Disable SPI Communication to  MCP23S08

	return n;
}

/*****************************************************************
 Function Name:    MCP23S08SetGPIObits
 Return Value:
 Parameters:       output port value, mask
 Description:      Change output value masked by mask.
 *******************************************************************/
void MCP23S08SetGPIObits(unsigned char port_data, unsigned char mask)
{
	unsigned char val = MCP23S08Read(GPIO);
	val = (val & (~mask)) | port_data;
	MCP23S08Write(GPIO, val);
}

/*****************************************************************
 Function Name:    MCP23S08SetDIR
 Return Value:
 Parameters:       port direction, mask
 Description:      Change port direction (0 - output/ 1 - input) masked by mask.
 *******************************************************************/
void MCP23S08SetDIR(unsigned char dir, unsigned char mask)
{
	unsigned char val = MCP23S08Read(IODIR);
	MCP23S08Write(IODIR, (val & (~mask)) | dir);
}
/*****************************************************************
 Function Name:    MCP23S08Init
 Return Value:
 Parameters:       port direction, pullup enable , initial port output state
 Description:      Initialize procedure for MCP23S08.
 *******************************************************************/

void MCP23S08Init(unsigned char dir, unsigned char pulup, unsigned char port_out)
{
	MCP23S08Write(GPPU, pulup);
	delayMicroseconds(10);			// hang out for 10 microseconds
	MCP23S08Write(GPIO, port_out);		//power ON(logic zero)
	MCP23S08Write(OLAT, port_out);
	delayMicroseconds(10);			// hang out for 10 microseconds
	MCP23S08Write(IOCON, 0x04);		//Set IRQ open Drain
	delayMicroseconds(10);			// hang out for 10 microseconds

	MCP23S08Write(IODIR, dir);		// Port Pin Directions

}
