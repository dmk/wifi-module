/*
 * spidev.h
 *
 *  Created on: Jul 21, 2017
 *      Author: admin
 */

#ifndef SPIDEV_H_
#define SPIDEV_H_

#define NONE_CS	0
#define EXT2_CS	1
#define OLED_CS	2
#define PORT_CS	3
#define CS_DECODER_PIN1	15
#define CS_DECODER_PIN2	16

void selectSpiDev(unsigned char cs_dev);
void initLbotSPI(void);
unsigned char transferSpiDev(unsigned char write_data, unsigned char mode);

#endif /* SPIDEV_H_ */

