/*
 * LbotConfig.cpp
 *
 *  Created on: Mar 8, 2018
 *      Author: dkunin
 */

#include <string.h>
#include "LbotConfig.h"
#include "LbotEeprom.h"
#include "crc16.h"
#include "debugMacros.h"
#include "str_tools.h"

LbotConfig::LbotConfig() {
}

void LbotConfig::begin(void) {
	if(sizeof(m_config_image)>512)
	{
		Serial.print("EEPROM image size more then 512 -> ");
		Serial.println(sizeof(m_config_image));
	}

	eeprom.begin();
	eeprom.read((char*)&m_config_image,0,sizeof(m_config_image));

	if(crc16((unsigned char*)&m_config_image, sizeof(m_config_image)))
	{
		DPRINTLN("EEPROM CRC ERROR ");
		set_default();
		save();
	}
//		strcpy(m_config_image.ssid,"linksys");
//		strcpy(m_config_image.passw,"2wr46yi8");

	//	strcpy(m_config_image.ssid,"fwe251");
	//	strcpy(m_config_image.passw,"1qe35tu7");

	//	strcpy(m_config_image.ssid,"antoshka");
	//	strcpy(m_config_image.passw,"antonkunin");

	//	strcpy(m_config_image.host_name,"garage-monitor");
	//	strcpy(m_config_image.host_name,"lbot");
}

void LbotConfig::set_default(void) {

	m_config_image.static_ip_enable = 0; //Disabled
	m_config_image.ip = IPAddress(192,168,1,200);
	m_config_image.mask = IPAddress(255,255,255,0);
	m_config_image.gate = IPAddress(192,168,1,1);
	m_config_image.softAp = 0;
	strcpy(m_config_image.data_server_host,"mosquitto.local");
	strcpy(m_config_image.topic_path,"test");
	strcpy(m_config_image.data_server_username, "");
	strcpy(m_config_image.data_server_pass, "");
	m_config_image.data_server_port = 1883;
	strcpy(m_config_image.host_name,"VL667_000");
	strcpy(m_config_image.ssid,"MMTEST");
	strcpy(m_config_image.passw,"12345678");
	strcpy(m_config_image.ssid1,"MMTEST1");
	strcpy(m_config_image.passw1,"12345678");
	strcpy(m_config_image.ssid2,"MMTEST2");
	strcpy(m_config_image.passw2,"12345678");

}

void LbotConfig::setGate(char *gate){
}

void LbotConfig::setIp(char *ip){
}

void LbotConfig::setMask(char *mask){
}

void LbotConfig::setGate(unsigned int gate){
	m_config_image.gate = gate;
}

void LbotConfig::setIp(unsigned int ip){
	m_config_image.ip = ip;
//	DPRINT("SetIP ");
//	DPRINTLN(m_config_image.ip);
}

void LbotConfig::setMask(unsigned  int mask){
	m_config_image.mask = mask;
}

void LbotConfig::setPassw(char *passw){
	if(strlen(passw) >sizeof(m_config_image.passw))
		return;
	strcpy(m_config_image.passw,passw);
}
void LbotConfig::setPassw(char *passw, int len ){
	if(len >sizeof(m_config_image.passw))
		return;

	//	Serial.print("setPassw ");

	strncpy(m_config_image.passw, passw, len);
	m_config_image.passw[len] = 0x0;
	//	Serial.println(m_config_image.passw);

}

void LbotConfig::setSsid(char *ssid){

	if(strlen(ssid) > sizeof(m_config_image.ssid))
		return;
	Serial.printf("SSID set to %s\n",ssid);

	strcpy(m_config_image.ssid,ssid);
}
void LbotConfig::setSsid(char *ssid, int len ){
	if(len >sizeof(m_config_image.ssid))
		return;

	//	Serial.print("setSsid :");

	strncpy(m_config_image.ssid,ssid, len);
	m_config_image.ssid[len] = 0x0;
	//	Serial.println(m_config_image.ssid);

}

void LbotConfig::setHostName(char *host_name){
	if(strlen(host_name) >sizeof(m_config_image.host_name))
		return;
	strcpy(m_config_image.host_name,host_name);
}
void LbotConfig::setHostName(char *host_name, int len ){
	if(len >sizeof(m_config_image.host_name))
		return;

	DPRINT("hostName:");

	strncpy(m_config_image.host_name, host_name, len);
	m_config_image.host_name[len] = 0x0;
	DPRINTLN(m_config_image.host_name);
}

void LbotConfig::setDataServerHost(char *data_server_host, int len ){
	if(len >sizeof(m_config_image.data_server_host)) {
		return;
	}
	DPRINT("data_server_host:");
	strncpy(m_config_image.data_server_host, data_server_host, len);
	m_config_image.data_server_host[len] = 0x0;
	DPRINTLN(m_config_image.data_server_host);
}

void LbotConfig::setDataServerUsername(char *data_server_username, int len ){
	if(len >sizeof(m_config_image.data_server_username)) {
		return;
	}
	DPRINT("data_server_username:");
	strncpy(m_config_image.data_server_username, data_server_username, len);
	m_config_image.data_server_username[len] = 0x0;
	DPRINTLN(m_config_image.data_server_username);
}

void LbotConfig::setDataServerPass(char *data_server_pass, int len ){
	if(len >sizeof(m_config_image.data_server_username)) {
		return;
	}
	DPRINT("data_server_pass:");
	strncpy(m_config_image.data_server_pass, data_server_pass, len);
	m_config_image.data_server_pass[len] = 0x0;
	DPRINTLN(m_config_image.data_server_pass);
}

void LbotConfig::setDataServerPort(int data_server_port){
	if(data_server_port > 0 && data_server_port <= 65535) {
		m_config_image.data_server_port = data_server_port;
	}
}

void LbotConfig::setTopicPath(char *topic_path, int len ){
	if(len >sizeof(m_config_image.topic_path)) {
		return;
	}
	strncpy(m_config_image.topic_path, topic_path, len);
	m_config_image.topic_path[len] = 0x0;
}

void LbotConfig::setSoftAp(boolean wifiHs ){
	m_config_image.softAp = wifiHs ? 1 : 0;
}

void LbotConfig::setStaticIpEnable(byte staticIpEnable) {
	m_config_image.static_ip_enable = staticIpEnable ? 1 : 0;
}

void LbotConfig::save(void)
{

	unsigned short crc_val;
	int image_size = sizeof(m_config_image);
	//	dumpbuff((char*)&m_config_image, sizeof(m_config_image));
	crc_val = crc16((unsigned char*)&m_config_image, image_size- 2);

	((unsigned char*)&m_config_image)[image_size-2] = crc_val&0xff;
	((unsigned char*)&m_config_image)[image_size-1] = (crc_val>>8)&0xff;
	//	if(crc16((unsigned char*)&m_config_image, image_size))
	//	{
	//		Serial.println("EEPROM CRC error ");
	//	}

	eeprom.write((char*)&m_config_image,0,image_size);
}


void LbotConfig::read(void)
{
	eeprom.read((char*)&m_config_image,0,sizeof(m_config_image));
	if(crc16((unsigned char*)&m_config_image, sizeof(m_config_image)))
	{
		DPRINTLN("EEPROM read error(CSum)");
	}

}

