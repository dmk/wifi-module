/*
 * board_conf.h
 *
 *  Created on: Feb 13, 2017
 *      Author: admin
 */

#ifndef BOARD_CONF_H_
#define BOARD_CONF_H_


/* Available device configurations *
 *
 * select  one of available
 * device configurations
 *
 * */

//#define HOUSE_MONITOR
//#define WEATHER_STATION
//#define DOOR_BELL
#define PROTOCOL_CONVERTER
//#define DEBUG
/************************
 * House Monitor module
 ************************/
#ifdef HOUSE_MONITOR

#define OLED 1
#define TEMPERATURE 1
#define PRESSURE 1
#define HUMIDITY 1

#define DEVICE_MODEL "LBoT"
#define DEVICE_NAME "House Monitor"
#define DEVICE_VERSION "1.0.4"

//#define HOSTING NONE
#define HOSTING PUBSUB
#endif // HOUSE_MONITOR

/************************
 * Weather station module
 ************************/
#ifdef WEATHER_STATION

#define OLED 1
#define TEMPERATURE 1
#define PRESSURE 0
#define HUMIDITY 0

#define DEVICE_MODEL "LBoT"
#define DEVICE_NAME "Weather Station"
#define DEVICE_VERSION "1.0.0"

#define HOSTING THINGSPEAK
#endif // WEATHER_STATION

/************************
 * Door bell module
 ************************/
#ifdef DOOR_BELL

#define OLED 0
#define TEMPERATURE 0
#define PRESSURE 1
#define HUMIDITY 1

#define DEVICE_MODEL "LBoT"
#define DEVICE_NAME "Door Bell"
#define DEVICE_VERSION "1.0.0"

//#define HOSTING THINGSPEAK
#define HOSTING PUBSUB
#endif //DOOR_BELL

/************************
 * Protocol converter
 ************************/
#ifdef PROTOCOL_CONVERTER
#define OLED 0
#define TEMPERATURE 0
#define PRESSURE 0
#define HUMIDITY 0

#define DEVICE_MODEL "VL-667"
#define DEVICE_NAME "Protocol converter"
#define DEVICE_VERSION "1.0.3"

#endif //PROTOCOL_CONVERTER

/********************************
* Do not edit this section		*
********************************/

/* Available hosting providers */
#define NONE	0
#define THINGSPEAK	1
#define PUBSUB	2


#if (OLED == 1)
/* Select Port Extender and OLED controller if OLED == 1 */
//#define SSD1325
#define SSD1305
#define MCP23S08
#endif


/* Pins definitions */
#if (OLED == 1)
#define RELAY_OUT 1  //EIO1
#else
#define RELAY_OUT	12	//GPIO12
#define DO0			15	//GPIO15
#define DO1			13	//GPIO13
#define DI0			0	//GPIO0
#define DI1			2	//GPIO2



#endif



#endif /* BOARD_CONF_H_ */


