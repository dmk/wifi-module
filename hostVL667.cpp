/*
 * hostVL667.cpp
 *
 *  Created on: Dec 23, 2019
 *      Author: admin
 */

#include "Arduino.h"
#include <stdio.h>
#include "board_conf.h"
#include "fw_ver.h"
#include "font.h"
#include "types.h"
#include "IoT_HostInteface.h"
#include "LbotConfig_WebInterface.h"
#include "TCP_Server.h"
#include "LbotConfig.h"
#include "LbotSensorReading.h"
#include "spidev.h"
#include "str_tools.h"
#include "LbotGpio.h"
#include "crc16.h"
#include "debugMacros.h"
#include "hostVL667.h"

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>

extern IPAddress myIP;
extern char WiFi_rssi;
extern int Serial_mutex;
extern int activatedSta; //is STA active
static File tempLog;

extern LbotConfig lbotconfig;
extern int update_host;


host_VL667::host_VL667()
{
	// TODO Auto-generated constructor stub

}

void host_VL667::update(void)
{
	static long last;

	if(millis()-last > 5000/*5 sec*/)
	{
		last = millis();
		send_connection_parameters();
		readCommandRegister();
	}
}
void host_VL667::send_connection_parameters(void)
{
}

void host_VL667::readCommandRegister(void)
{
}

#ifdef	PROTOCOL_CONVERTER

void FlushFromSerial()
{
	while(Serial.available()) {
		char ch;
		Serial.readBytes(&ch, 1);
		DPRINT(".");
	}
	delay(25);

}

uint8_t buildProtocolRequest(PROTOCOL_CMD_t cmd, uint8_t *buf, uint8_t address, uint16_t param_addr, uint8_t *data, uint8_t datalen)
{
	uint8_t len = 0;
	buf[len] = address | (uint8_t)cmd;
	len++;
	buf[len] = datalen;
	len++;
	*((uint16_t*)&buf[len]) = param_addr;
	len += 2;

	if(CMD_WRITE == cmd)
	{
		while (len < (datalen+4))
		{
			buf[len] = *data;
			data++;
			len++;
		}
	}
	else
	{
		buf[len] = *data;//Number of parameters for read
		len++;
	}
//	Serial.println(len);
//	*((unsigned short*)&buf[len]) = crc16(buf, len);
	unsigned short result = crc16(buf, len);
	buf[len] = ((uint8_t*)&result)[0];
	buf[len+1] = ((uint8_t*)&result)[1];
//	Serial.println(len);
	len += 2;/*CRC*/
#ifdef LOG_ENA
	tempLog.print("Send len = ");
	tempLog.println(len);
	dumpbuff((char *)buf, len, tempLog);
#endif

	return len;
}

void send_ip(void)
{
	uint8_t sbuf[128]={0};
	uint8_t len = 0;

	FlushFromSerial();

	uint32_t ip_buf_data = myIP;
//	DPRINTF("%d,%d,%d,%d \n",((uint8_t*)&ip_buf_data)[0],((uint8_t*)&ip_buf_data)[1],((uint8_t*)&ip_buf_data)[2],((uint8_t*)&ip_buf_data)[3]);
	len = buildProtocolRequest(CMD_WRITE, sbuf, 0, 36, (uint8_t*)&ip_buf_data, 4);  //for IP
	Serial.write(sbuf,len);
//	dumpbuff((char*)sbuf, len);
}

void send_connection_parameters(void)
{
	uint8_t sbuf[128]={0};
	uint8_t len;

	FlushFromSerial();

	uint16_t data = ((0xFF & WiFi_rssi)<<8) | (0xFF & activatedSta);//

	len = buildProtocolRequest(CMD_WRITE, sbuf, 0, 37, (uint8_t*)&data, 2);  //for IP
	Serial.write(sbuf,len);
//	dumpbuff((char*)sbuf, len+6);
}
void readCommandRegister(void)
{
	uint8_t sbuf[128]={0};
	uint8_t len = 0;
	FlushFromSerial();

	uint8_t data = 1;//Number of params for read

	len = buildProtocolRequest(CMD_READ, sbuf, 0, 37, (uint8_t*)&data, 1/*1 - for CMD_READ*/);
	Serial.write(sbuf,len);
}
void readNetParameters(void)
{
	uint8_t sbuf[128]={0};
	uint8_t len = 0;
	FlushFromSerial();

	uint8_t data = 1;//Number of params for read

	len = buildProtocolRequest(CMD_READ, sbuf, 0, 38, (uint8_t*)&data, 1/*1 - for CMD_READ*/);
	Serial.write(sbuf,len);
}
void readWifiParameters(void)
{
	uint8_t sbuf[128]={0};
	uint8_t len = 0;
	FlushFromSerial();

	uint8_t data = 1;//Number of params for read

	len = buildProtocolRequest(CMD_READ, sbuf, 0, 39, (uint8_t*)&data, 1/*1 - for CMD_READ*/);
	Serial.write(sbuf,len);
}

enum DataType_t
{
    ENUM_DATATYPE_BYTE = 1, //1 byte
    ENUM_DATATYPE_SHORT = 2, //2 byte
    ENUM_DATATYPE_INT = 4 //4 byte
};
enum ErrCode_t
{
    ENUM_OK,
    ENUM_ERR
};

#define	ADDR_MASK			0x3f //
#define	CMD_MASK			0x40 //
#define	READ_CMD			0x00 // read command
#define	WRITE_CMD			0x40 // write command
#define	HEADER_LEN		4 		//length of message header


//ErrCode_t writeRegister2host(uint8_t device_address, uint16_t reg_address,void *data, DataType_t data_type)
//{
//	ErrCode_t ret = ENUM_OK;
//	uint8_t sbuf[128]={0};
//	uint8_t len = 0;
//
//	if(device_address > 0x3F)
//	{
//		return ENUM_ERR;
//	}
//	sbuf[0] = WRITE_CMD | device_address; //device address, CMD write
//
//	*((short*)&sbuf[2]) = reg_address;
//	*((int*)&sbuf[4]) = *(int*)data;
//	if(data_type > ENUM_DATATYPE_INT)
//	{
//		return ENUM_ERR;
//	}
//	len = (uint8_t)data_type;
//	sbuf[1] = len;
//	*((short*)&sbuf[len + HEADER_LEN]) = crc16(sbuf, len + HEADER_LEN);
//
//	Serial.write(sbuf,len + HEADER_LEN + 2/*CRC*/);
//	return ret;
//}



int receiveResponse(uint8_t *buf)
{
	size_t len = 0;
	size_t ret_len = 0;
	char *p_buf = (char *)buf;
	long last_received = millis();
	long timeout = 200L;

	while(Serial.available() || (millis()-last_received < timeout))
	{
		if (Serial.available()) {
			last_received = millis();
			len = Serial.available();
			Serial.readBytes(buf, len);
			buf += len;
			ret_len += len;
			timeout = 40L;
		}
		else
		{
			delay(10);
		}
	}
#ifdef LOG_ENA
	tempLog.print("Receive len = ");
	tempLog.println(ret_len);
	dumpbuff(p_buf, ret_len, tempLog);
#endif
	return ret_len;
}
int process(uint8_t *buf,uint8_t *data,uint8_t max_len)
{
	uint8_t len = 0;

#ifdef LOG_ENA
	tempLog.println("Process");
#endif
	if(*buf & 0x80) //if any error
	{
#ifdef LOG_ENA
		tempLog.println("Error responce");
#endif
		return -1;
	}
	len = *(uint8_t*)(buf+1);
	if(crc16(buf, len + HEADER_LEN + 2/*CRC*/))
	{
#ifdef LOG_ENA
		tempLog.println("CRC Error");
#endif
		return -1;
	}
	buf+=2;
	uint8_t cntr = 0;
	buf+=2;
	while(cntr < len)
	{
		if((0 != data)&&(max_len > cntr))
		{
			*data = *buf;
			data++;
		}
		buf++;
		cntr++;
	}
//	DPRINT("Processed ");
//	DPRINTLN(len);
	if(0 == max_len)
		return 0;

	return cntr;
}

typedef struct
{
	unsigned int ip;
	unsigned int mask;
	unsigned int gate;
	byte gap;
	byte dhcp;

} netParam_t;


int updateHostConnectionStatus (uint8_t *data, File logfile)
{
	int len = 0;
	int ret = -1;

#ifdef LOG_ENA
	logfile.println("Send Status");
#endif
	send_connection_parameters();
	len = receiveResponse(data);

	if(len)
	{
		if ( 0 <= process(data,0,0))
		{
#ifdef LOG_ENA
			logfile.println("Conn Status Updated");
#endif
			ret = 0;
		}else
		{
#ifdef LOG_ENA
			logfile.println(" Error update conn Status");
#endif
			ret = -1;
		}
	}
	return ret;
}




int updateHostIP(uint8_t *data, File logfile)
{
	int len = 0;
	int ret = -1;

#ifdef LOG_ENA
	logfile.println("Send IP Data");
#endif
	send_ip();
	len = receiveResponse(data);

	if(len)
	{
		if ( 0 <= process(data,0,0))
		{
#ifdef LOG_ENA
			logfile.println("IP updated");
#endif
			ret = 0;
		}else
		{
#ifdef LOG_ENA
			logfile.println(" Error Update IP");
#endif
			ret = -1;
		}
	}
	return ret;
}

int updateNetParams(uint8_t *data, File logfile)
{
	int len = 0;
	int ret = -1;
	uchar data_buffer[128];

#ifdef LOG_ENA
	logfile.println("Read Net Data");
#endif
	readNetParameters();
	len = receiveResponse(data);
	if(len)
	{
		if ( 0 < process(data,(unsigned char*)data_buffer,128))
		{
#ifdef LOG_ENA
			logfile.print("Net Data ");
			logfile.println(*data_buffer);
#endif
			lbotconfig.setIp(((netParam_t*)data_buffer)->ip);
			lbotconfig.setMask(((netParam_t*)data_buffer)->mask);
			lbotconfig.setGate(((netParam_t*)data_buffer)->gate);
			lbotconfig.setStaticIpEnable(((netParam_t*)data_buffer)->dhcp);
			ret = 0;
		}else
		{
#ifdef LOG_ENA
			logfile.print(" Error Data");
#endif
			ret = -1;
		}
	}
	return ret;
}


int updateWiFIParams(uint8_t *data, File logfile)
{
	int len = 0;
	int ret = -1;
	uchar data_buffer[128];

#ifdef LOG_ENA
	logfile.println("Read Wifi Data");
#endif
	readWifiParameters();
	len = receiveResponse(data);
	if(len)
	{
		if ( 0 < process(data,(unsigned char*)data_buffer,128))
		{
#ifdef LOG_ENA
			logfile.print("Wifi Data ");
			logfile.println(*data_buffer);
#endif
			lbotconfig.setSsid((char*)data_buffer);
			lbotconfig.setPassw((char*)(data_buffer+16));
			lbotconfig.setHostName((char*)(data_buffer+32));
			ret = 0;
		}else{
#ifdef LOG_ENA
			logfile.print(" Error Data");
#endif
			ret = -1;
		}
	}
	return ret;
}


int waitForHostToStart()
{
//	static long last;
	uint8_t response[256];
	int ret = -1;

	if(Serial_mutex == 1)
		return ret;
	Serial_mutex = 1;

#ifdef LOG_ENA
	tempLog = SPIFFS.open("/temp.log", "a+"); // Write to the log file
	tempLog.print(" Wait for VL667");
#endif
	while(1)
	{
#if 1

		ret = updateNetParams(response, tempLog);
		if(!ret)
		{
			ret = updateWiFIParams(response, tempLog);
		}
		if(!ret)
		{
#ifdef LOG_ENA
			tempLog.print("VL667 has been stared");
#endif
			break;
		}
		delay(20);

#else




//		uchar data_buffer[128];
		readNetParameters();
		len = receiveResponse(response);
//		Serial.printf("readNetParameters len %d  \n",len);
		if (len == 20)
		{
			if ( 0 == process(response,0,0))
			{
				tempLog.print("VL667 has been stared");
				break;
			}else{
				tempLog.print(" Wait for VL667");
			}
		}
		delay(20);
#endif
	}
#ifdef LOG_ENA
	tempLog.println(" ");
	tempLog.close();
#endif
	Serial_mutex = 0;
	return ret;

}

typedef enum eUPDATE_STATE
{
	STATUS_UPDATE,
	NETPARAM_UPDATE,
	WIFIPARAM_UPDATE
};

int updateHost(int enforceFlg)
{
	static long last = 0;
	static eUPDATE_STATE updateState = STATUS_UPDATE;
	uint8_t response[256];
	int ret = 0;

	if(Serial_mutex == 1)
		return -1;
	Serial_mutex = 1;

	if ((enforceFlg = 1) || (millis()-last > 5000/*5 sec*/))
	{
		update_host++;
		DPRINT("Open /temp.log ");
#ifdef LOG_ENA
		tempLog = SPIFFS.open("/temp.log", "a+"); // Write to the log file
		DPRINTLN(tempLog);
		tempLog.print(':');
		tempLog.println(millis()/1000);
#endif
		last = millis();
		switch(updateState)
		{
			case STATUS_UPDATE:
				updateState = NETPARAM_UPDATE;
				if(!ret)
				{
					ret = updateHostIP(response, tempLog);
				}
				if(!ret)
				{
					ret = updateHostConnectionStatus(response, tempLog);
				}
				break;
			case NETPARAM_UPDATE:
				updateState = WIFIPARAM_UPDATE;
				if(!ret)
				{
					ret = updateNetParams(response, tempLog);
				}
				break;
			case WIFIPARAM_UPDATE:
				updateState = STATUS_UPDATE;
				if(!ret)
				{
					ret = updateWiFIParams(response, tempLog);
				}
				break;
			default:
				break;
		}



//		uint16_t data = 0;
//		readCommandRegister();
//		len = receiveResponse(response);
//		if(len)
//		{
//
//			if ( 0 == process(response,(unsigned char*)&data,2))
//			{
//				tempLog.print("Data ");
//				tempLog.println(data);
//			}else{
//				tempLog.print(" Error Data");
//			}
//		}


#ifdef LOG_ENA
		tempLog.println(" ");
		tempLog.close();
#endif
		Serial_mutex = 0;

	}
	return ret;
}



#if 0
#define	ADDR_MASK			0x3f //
#define	CMD_MASK			0x40 //
#define	READ_CMD			0x00 // read command
#define	WRITE_CMD			0x40 // write command
#define	READCMD_LEN		1
#define	HEADER_LEN		4 		//length of message header

typedef struct MESSAGE_HDR
{
	unsigned char MesCmd;
	unsigned char MesLen;
	unsigned int MesAddr;
	unsigned char MesData;
} MESSAGE_HDR;





unsigned char process_req(unsigned char *RxBufPtr, unsigned char len, unsigned char *TxBufPtr)
{

	unsigned char cmd;
	unsigned char response_len;
//	volatile unsigned char err = 0;
	unsigned char deviceAddr;

	if(crc16(RxBufPtr, len)) /*Checksum*/
	{
//			err++;
		return 0;
	}
	cmd = ((MESSAGE_HDR*)RxBufPtr)->MesCmd; /*Get Message Command*/
	((MESSAGE_HDR*)TxBufPtr)->MesCmd = cmd; /*Copy request command to TX buffer*/
	deviceAddr = cmd & (~ADDR_MASK);
	cmd &= CMD_MASK;

	if((deviceAddr != 0) && (if_address != deviceAddr))
		return 0;

	if(cmd == READ_CMD) /*Read*/
	{
		response_len = proc_RD(RxBufPtr, TxBufPtr);
	}
	else if(cmd == WRITE_CMD) /*Write*/
	{
		response_len = proc_WR(RxBufPtr, len, TxBufPtr);
	}

	((MESSAGE_HDR*)TxBufPtr)->MesLen = response_len; /*Set Message len to TX buffer*/
	*((unsigned short*)&TxBufPtr[response_len + HEADER_LEN]) = crc16(TxBufPtr, response_len + HEADER_LEN);

	return response_len + HEADER_LEN + 2;
} //func




#endif

#endif

