/*
 * LbotEeprom.cpp

 *
 *  Created on: Mar 8, 2018
 *      Author: dkunin
 */

#include "LbotEeprom.h"
#include <EEPROM.h>

LbotEeprom::LbotEeprom() {
}

void LbotEeprom::begin(void) {
	EEPROM.begin(EEPROM_SIZE);
}

byte value;
uchar LbotEeprom::read(char *ptr,int idx, int len) {
	if(idx + len > EEPROM_SIZE)
		return 0;
//	Serial.println("Reading EEPROM");
	for (int i = idx; i < len+idx; ++i){

		ptr[i] = (char)EEPROM.read(i);
		delay(10);
//		Serial.print(ptr[i]);
//		Serial.print("(");
//		Serial.print(ptr[i],DEC);
//		Serial.print(")");
//		Serial.print(",");
	}
//	Serial.println(".");
	return 1;
}

uchar LbotEeprom::write(char *ptr,int idx, int len) {
	if(idx + len > EEPROM_SIZE)
		return 0;
//	Serial.println("Writing EEPROM");

//	EEPROM.put(0,ptr);
//	delay(500);

	for (int i = idx; i < len+idx; ++i) {
		EEPROM.write(i, ptr[i]);
		delay(10);

//		Serial.print(ptr[i]);
//		Serial.print(",");
	}
//	Serial.println(".");

	EEPROM.commit();
	delay(50);
	return 1;
}
