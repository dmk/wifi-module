/*
 * LbotGpio.cpp
 *
 *  Created on: May 19, 2018
 *      Author: admin
 */

#include "Arduino.h"
#include "LbotGpio.h"
#include "board_conf.h"
#if (OLED != 0)
#include "MCP23S08.h"
#endif// OLED


LbotGpio::LbotGpio()
{
	// TODO Auto-generated constructor stub

}
void LbotGpio::begin(void)
{
#if (OLED != 0)
	MCP23S08SetGPIObits(0, 1<<RELAY_OUT);
	MCP23S08SetDIR(0, 1<<RELAY_OUT);
#else
	pinMode(DI0, INPUT);
	pinMode(DI1, INPUT);
	digitalWrite(DO0, LOW);
	pinMode(DO0, OUTPUT);
	digitalWrite(DO1, LOW);
	pinMode(DO1, OUTPUT);

#endif



}

byte LbotGpio::getDI0() {
#if (OLED != 0)
	return (MCP23S08Read(GPIO)>>DO0)&0x01;
#else
	return digitalRead(DI0);
#endif
}

byte LbotGpio::getDI1() {
#if (OLED != 0)
	return (MCP23S08Read(GPIO)>>DO1)&0x01;
#else
	return digitalRead(DI1);
#endif
}

void LbotGpio::setDO0() {
#if (OLED != 0)
    MCP23S08SetGPIObits(1<<RELAY_OUT,1<<RELAY_OUT);
#else
	digitalWrite(DO0, HIGH);
#endif
}

void LbotGpio::setDO1() {
#if (OLED != 0)
	MCP23S08SetGPIObits(0, 1<<RELAY_OUT);
#else
	digitalWrite(DO1, HIGH);
#endif
}

void LbotGpio::clrDO0() {
#if (OLED != 0)
    MCP23S08SetGPIObits(1<<RELAY_OUT,1<<RELAY_OUT);
#else
	digitalWrite(DO0, LOW);
#endif
}

void LbotGpio::clrDO1() {
#if (OLED != 0)
	MCP23S08SetGPIObits(0, 1<<RELAY_OUT);
#else
	digitalWrite(DO1, LOW);
#endif
}

byte LbotGpio::pollDI(byte &portVal) {

	byte maskChangedBits = 0;
	static byte oldPortVal = 0;
	portVal = (digitalRead(DI1)<<INPUT_PORT1) | digitalRead(DI0);

	if(portVal != oldPortVal)
	{
		maskChangedBits = oldPortVal ^ portVal;
		oldPortVal = portVal;
//		Serial.print(maskChangedBits);
	}
	return maskChangedBits;
}

