#include "types.h"
#include "font.h"
/* 
 **  Font data for Miriam 10pt
 */

/* Character bitmaps for Miriam 10pt */
const uchar miriam10ptBitmaps[] =
{
	/* @0 ' ' (2 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */

	/* @12 '!' (2 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */

	/* @24 '"' (4 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x0F, /*     #### */
	0x0F, /*     #### */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */

	/* @36 '#' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x78, /*  ####    */
	0x78, /*  ####    */
	0x78, /*  ####    */
	0xFF, /* ######## */
	0x3C, /*   ####   */
	0xFF, /* ######## */
	0x1E, /*    ####  */
	0x1E, /*    ####  */
	0x00, /*          */
	0x00, /*          */

	/* @48 '$' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x18, /*    ##    */
	0x3C, /*   ####   */
	0x7E, /*  ######  */
	0x1E, /*    ####  */
	0x3C, /*   ####   */
	0x78, /*  ####    */
	0x7B, /*  #### ## */
	0x3E, /*   #####  */
	0x18, /*    ##    */
	0x00, /*          */

	/* @60 '%' (9 pixels wide) */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */
	0x60, 0x00, /*  ##              */
	0x3E, 0x00, /*   #####          */
	0x3B, 0x00, /*   ### ##         */
	0x1B, 0x00, /*    ## ##         */
	0xFE, 0x00, /* #######          */
	0xBC, 0x01, /* # ####         # */
	0xB6, 0x01, /* # ## ##        # */
	0xE6, 0x00, /* ###  ##          */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */

	/* @84 '&' (9 pixels wide) */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */
	0x1C, 0x00, /*    ###           */
	0x36, 0x00, /*   ## ##          */
	0x1E, 0x00, /*    ####          */
	0xEC, 0x00, /* ### ##           */
	0x7E, 0x00, /*  ######          */
	0x7B, 0x00, /*  #### ##         */
	0x33, 0x00, /*   ##  ##         */
	0xFF, 0x01, /* ########       # */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */

	/* @108 ''' (2 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */

	/* @120 '(' (3 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x06, /*      ##  */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x06, /*      ##  */
	0x00, /*          */

	/* @132 ')' (3 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */

	/* @144 '*' (6 pixels wide) */
	0x0C, /*     ##   */
	0x3F, /*   ###### */
	0x1E, /*    ####  */
	0x3F, /*   ###### */
	0x0C, /*     ##   */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */

	/* @156 '+' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0xFF, /* ######## */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x00, /*          */
	0x00, /*          */

	/* @168 ',' (3 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x06, /*      ##  */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */

	/* @180 '-' (4 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x0F, /*     #### */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */

	/* @192 '.' (2 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */

	/* @204 '/' (5 pixels wide) */
	0x00, /*          */
	0x18, /*    ##    */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */

	/* @216 '0' (5 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x0E, /*     ###  */
	0x1B, /*    ## ## */
	0x1B, /*    ## ## */
	0x1B, /*    ## ## */
	0x1B, /*    ## ## */
	0x1B, /*    ## ## */
	0x0E, /*     ###  */
	0x00, /*          */
	0x00, /*          */

	/* @228 '1' (4 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x0E, /*     ###  */
	0x0F, /*     #### */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x00, /*          */
	0x00, /*          */

	/* @240 '2' (5 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x0E, /*     ###  */
	0x1B, /*    ## ## */
	0x18, /*    ##    */
	0x0C, /*     ##   */
	0x06, /*      ##  */
	0x03, /*       ## */
	0x1F, /*    ##### */
	0x00, /*          */
	0x00, /*          */

	/* @252 '3' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x3E, /*   #####  */
	0x18, /*    ##    */
	0x1C, /*    ###   */
	0x30, /*   ##     */
	0x30, /*   ##     */
	0x33, /*   ##  ## */
	0x1E, /*    ####  */
	0x00, /*          */
	0x00, /*          */

	/* @264 '4' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x1C, /*    ###   */
	0x1E, /*    ####  */
	0x1E, /*    ####  */
	0x1B, /*    ## ## */
	0x3F, /*   ###### */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x00, /*          */
	0x00, /*          */

	/* @276 '5' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x3E, /*   #####  */
	0x06, /*      ##  */
	0x1F, /*    ##### */
	0x30, /*   ##     */
	0x30, /*   ##     */
	0x33, /*   ##  ## */
	0x1E, /*    ####  */
	0x00, /*          */
	0x00, /*          */

	/* @288 '6' (5 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x0E, /*     ###  */
	0x03, /*       ## */
	0x0F, /*     #### */
	0x1B, /*    ## ## */
	0x1B, /*    ## ## */
	0x1B, /*    ## ## */
	0x0E, /*     ###  */
	0x00, /*          */
	0x00, /*          */

	/* @300 '7' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x3F, /*   ###### */
	0x30, /*   ##     */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x00, /*          */
	0x00, /*          */

	/* @312 '8' (5 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x0E, /*     ###  */
	0x1B, /*    ## ## */
	0x1B, /*    ## ## */
	0x0E, /*     ###  */
	0x1B, /*    ## ## */
	0x1B, /*    ## ## */
	0x0E, /*     ###  */
	0x00, /*          */
	0x00, /*          */

	/* @324 '9' (5 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x0E, /*     ###  */
	0x1B, /*    ## ## */
	0x1B, /*    ## ## */
	0x1B, /*    ## ## */
	0x1E, /*    ####  */
	0x18, /*    ##    */
	0x0E, /*     ###  */
	0x00, /*          */
	0x00, /*          */

	/* @336 ':' (2 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */

	/* @348 ';' (3 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x06, /*      ##  */
	0x00, /*          */
	0x00, /*          */
	0x06, /*      ##  */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */

	/* @360 '<' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x60, /*  ##      */
	0x38, /*   ###    */
	0x0E, /*     ###  */
	0x07, /*      ### */
	0x1C, /*    ###   */
	0x70, /*  ###     */
	0x00, /*          */
	0x00, /*          */

	/* @372 '=' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x7F, /*  ####### */
	0x00, /*          */
	0x7F, /*  ####### */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */

	/* @384 '>' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x0E, /*     ###  */
	0x38, /*   ###    */
	0x70, /*  ###     */
	0x1C, /*    ###   */
	0x07, /*      ### */
	0x00, /*          */
	0x00, /*          */

	/* @396 '?' (5 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x0E, /*     ###  */
	0x1B, /*    ## ## */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x00, /*          */
	0x0C, /*     ##   */
	0x00, /*          */
	0x00, /*          */

	/* @408 '@' (11 pixels wide) */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */
	0xF8, 0x01, /* #####          # */
	0x8C, 0x03, /* #   ##        ## */
	0xF6, 0x07, /* #### ##      ### */
	0xDB, 0x07, /* ## ## ##     ### */
	0xCF, 0x06, /* ##  ####     ##  */
	0xCF, 0x06, /* ##  ####     ##  */
	0xEF, 0x03, /* ### ####      ## */
	0xFE, 0x07, /* #######      ### */
	0x0E, 0x03, /*     ###       ## */
	0xF8, 0x01, /* #####          # */

	/* @432 'A' (9 pixels wide) */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */
	0x38, 0x00, /*   ###            */
	0x38, 0x00, /*   ###            */
	0x6C, 0x00, /*  ## ##           */
	0x6C, 0x00, /*  ## ##           */
	0x7E, 0x00, /*  ######          */
	0xC6, 0x00, /* ##   ##          */
	0xC6, 0x00, /* ##   ##          */
	0x83, 0x01, /* #     ##       # */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */

	/* @456 'B' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x3F, /*   ###### */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x3F, /*   ###### */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x3F, /*   ###### */
	0x00, /*          */
	0x00, /*          */

	/* @468 'C' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x3C, /*   ####   */
	0x66, /*  ##  ##  */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x63, /*  ##   ## */
	0x66, /*  ##  ##  */
	0x3C, /*   ####   */
	0x00, /*          */
	0x00, /*          */

	/* @480 'D' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x1F, /*    ##### */
	0x33, /*   ##  ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x33, /*   ##  ## */
	0x1F, /*    ##### */
	0x00, /*          */
	0x00, /*          */

	/* @492 'E' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x7F, /*  ####### */
	0x03, /*       ## */
	0x03, /*       ## */
	0x3F, /*   ###### */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x7F, /*  ####### */
	0x00, /*          */
	0x00, /*          */

	/* @504 'F' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x3F, /*   ###### */
	0x03, /*       ## */
	0x03, /*       ## */
	0x3F, /*   ###### */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */

	/* @516 'G' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x7C, /*  #####   */
	0xC6, /* ##   ##  */
	0x03, /*       ## */
	0x03, /*       ## */
	0xFB, /* ##### ## */
	0xC3, /* ##    ## */
	0xC6, /* ##   ##  */
	0x7C, /*  #####   */
	0x00, /*          */
	0x00, /*          */

	/* @528 'H' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x7F, /*  ####### */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x00, /*          */
	0x00, /*          */

	/* @540 'I' (2 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */

	/* @552 'J' (5 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x1B, /*    ## ## */
	0x0E, /*     ###  */
	0x00, /*          */
	0x00, /*          */

	/* @564 'K' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x63, /*  ##   ## */
	0x33, /*   ##  ## */
	0x1B, /*    ## ## */
	0x0F, /*     #### */
	0x1F, /*    ##### */
	0x33, /*   ##  ## */
	0x63, /*  ##   ## */
	0xC3, /* ##    ## */
	0x00, /*          */
	0x00, /*          */

	/* @576 'L' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x3F, /*   ###### */
	0x00, /*          */
	0x00, /*          */

	/* @588 'M' (9 pixels wide) */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */
	0xC7, 0x01, /* ##   ###       # */
	0xC7, 0x01, /* ##   ###       # */
	0xEF, 0x01, /* ### ####       # */
	0xEF, 0x01, /* ### ####       # */
	0xEF, 0x01, /* ### ####       # */
	0xBB, 0x01, /* # ### ##       # */
	0xBB, 0x01, /* # ### ##       # */
	0xBB, 0x01, /* # ### ##       # */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */

	/* @612 'N' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x63, /*  ##   ## */
	0x67, /*  ##  ### */
	0x67, /*  ##  ### */
	0x6F, /*  ## #### */
	0x7B, /*  #### ## */
	0x7B, /*  #### ## */
	0x73, /*  ###  ## */
	0x63, /*  ##   ## */
	0x00, /*          */
	0x00, /*          */

	/* @624 'O' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x3C, /*   ####   */
	0x66, /*  ##  ##  */
	0xC3, /* ##    ## */
	0xC3, /* ##    ## */
	0xC3, /* ##    ## */
	0xC3, /* ##    ## */
	0x66, /*  ##  ##  */
	0x3C, /*   ####   */
	0x00, /*          */
	0x00, /*          */

	/* @636 'P' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x3F, /*   ###### */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x3F, /*   ###### */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */

	/* @648 'Q' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x3C, /*   ####   */
	0x66, /*  ##  ##  */
	0xC3, /* ##    ## */
	0xC3, /* ##    ## */
	0xC3, /* ##    ## */
	0xF3, /* ####  ## */
	0x66, /*  ##  ##  */
	0xFC, /* ######   */
	0x00, /*          */
	0x00, /*          */

	/* @660 'R' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x3F, /*   ###### */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x3F, /*   ###### */
	0x1B, /*    ## ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x63, /*  ##   ## */
	0x00, /*          */
	0x00, /*          */

	/* @672 'S' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x3E, /*   #####  */
	0x63, /*  ##   ## */
	0x03, /*       ## */
	0x3E, /*   #####  */
	0x60, /*  ##      */
	0x60, /*  ##      */
	0x63, /*  ##   ## */
	0x3E, /*   #####  */
	0x00, /*          */
	0x00, /*          */

	/* @684 'T' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x3F, /*   ###### */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x00, /*          */
	0x00, /*          */

	/* @696 'U' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x63, /*  ##   ## */
	0x3E, /*   #####  */
	0x00, /*          */
	0x00, /*          */

	/* @708 'V' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0xC3, /* ##    ## */
	0x66, /*  ##  ##  */
	0x66, /*  ##  ##  */
	0x66, /*  ##  ##  */
	0x3C, /*   ####   */
	0x3C, /*   ####   */
	0x3C, /*   ####   */
	0x18, /*    ##    */
	0x00, /*          */
	0x00, /*          */

	/* @720 'W' (12 pixels wide) */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */
	0x63, 0x0C, /*  ##   ##    ##   */
	0xF3, 0x0C, /* ####  ##    ##   */
	0xF6, 0x06, /* #### ##      ##  */
	0xF6, 0x06, /* #### ##      ##  */
	0x9E, 0x07, /* #  ####      ### */
	0x9E, 0x07, /* #  ####      ### */
	0x9E, 0x03, /* #  ####       ## */
	0x0C, 0x03, /*     ##        ## */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */

	/* @744 'X' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0xC3, /* ##    ## */
	0x66, /*  ##  ##  */
	0x3C, /*   ####   */
	0x18, /*    ##    */
	0x38, /*   ###    */
	0x3C, /*   ####   */
	0x66, /*  ##  ##  */
	0xC3, /* ##    ## */
	0x00, /*          */
	0x00, /*          */

	/* @756 'Y' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0xC3, /* ##    ## */
	0x66, /*  ##  ##  */
	0x3C, /*   ####   */
	0x3C, /*   ####   */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x18, /*    ##    */
	0x00, /*          */
	0x00, /*          */

	/* @768 'Z' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x3F, /*   ###### */
	0x30, /*   ##     */
	0x18, /*    ##    */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x06, /*      ##  */
	0x03, /*       ## */
	0x3F, /*   ###### */
	0x00, /*          */
	0x00, /*          */

	/* @780 '[' (3 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x07, /*      ### */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x07, /*      ### */
	0x00, /*          */

	/* @792 '\' (4 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x00, /*          */
	0x00, /*          */

	/* @804 ']' (3 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x07, /*      ### */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x07, /*      ### */
	0x00, /*          */

	/* @816 '^' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x0C, /*     ##   */
	0x1E, /*    ####  */
	0x1E, /*    ####  */
	0x1E, /*    ####  */
	0x33, /*   ##  ## */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */

	/* @828 '_' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0xFF, /* ######## */

	/* @840 '`' (3 pixels wide) */
	0x00, /*          */
	0x03, /*       ## */
	0x06, /*      ##  */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */

	/* @852 'a' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x1E, /*    ####  */
	0x33, /*   ##  ## */
	0x3E, /*   #####  */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x3E, /*   #####  */
	0x00, /*          */
	0x00, /*          */

	/* @864 'b' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x1F, /*    ##### */
	0x37, /*   ## ### */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x1F, /*    ##### */
	0x00, /*          */
	0x00, /*          */

	/* @876 'c' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x1E, /*    ####  */
	0x33, /*   ##  ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x33, /*   ##  ## */
	0x1E, /*    ####  */
	0x00, /*          */
	0x00, /*          */

	/* @888 'd' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x30, /*   ##     */
	0x30, /*   ##     */
	0x3E, /*   #####  */
	0x3B, /*   ### ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x3E, /*   #####  */
	0x00, /*          */
	0x00, /*          */

	/* @900 'e' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x1E, /*    ####  */
	0x33, /*   ##  ## */
	0x3F, /*   ###### */
	0x03, /*       ## */
	0x33, /*   ##  ## */
	0x1E, /*    ####  */
	0x00, /*          */
	0x00, /*          */

	/* @912 'f' (4 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x0E, /*     ###  */
	0x06, /*      ##  */
	0x0F, /*     #### */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x00, /*          */
	0x00, /*          */

	/* @924 'g' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x3E, /*   #####  */
	0x3B, /*   ### ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x3E, /*   #####  */
	0x33, /*   ##  ## */
	0x1E, /*    ####  */

	/* @936 'h' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x1F, /*    ##### */
	0x37, /*   ## ### */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x00, /*          */
	0x00, /*          */

	/* @948 'i' (2 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */

	/* @960 'j' (3 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x06, /*      ##  */
	0x00, /*          */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x03, /*       ## */

	/* @972 'k' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x1B, /*    ## ## */
	0x0F, /*     #### */
	0x07, /*      ### */
	0x0F, /*     #### */
	0x1B, /*    ## ## */
	0x33, /*   ##  ## */
	0x00, /*          */
	0x00, /*          */

	/* @984 'l' (2 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */

	/* @996 'm' (10 pixels wide) */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */
	0xFF, 0x01, /* ########       # */
	0x37, 0x03, /*   ## ###      ## */
	0x33, 0x03, /*   ##  ##      ## */
	0x33, 0x03, /*   ##  ##      ## */
	0x33, 0x03, /*   ##  ##      ## */
	0x33, 0x03, /*   ##  ##      ## */
	0x00, 0x00, /*                  */
	0x00, 0x00, /*                  */

	/* @1020 'n' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x1F, /*    ##### */
	0x37, /*   ## ### */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x00, /*          */
	0x00, /*          */

	/* @1032 'o' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x1E, /*    ####  */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x1E, /*    ####  */
	0x00, /*          */
	0x00, /*          */

	/* @1044 'p' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x1F, /*    ##### */
	0x37, /*   ## ### */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x1F, /*    ##### */
	0x03, /*       ## */
	0x03, /*       ## */

	/* @1056 'q' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x3E, /*   #####  */
	0x3B, /*   ### ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x3E, /*   #####  */
	0x30, /*   ##     */
	0x30, /*   ##     */

	/* @1068 'r' (4 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x0F, /*     #### */
	0x07, /*      ### */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */

	/* @1080 's' (5 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x1E, /*    ####  */
	0x03, /*       ## */
	0x0E, /*     ###  */
	0x18, /*    ##    */
	0x1B, /*    ## ## */
	0x0E, /*     ###  */
	0x00, /*          */
	0x00, /*          */

	/* @1092 't' (4 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x0F, /*     #### */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x0C, /*     ##   */
	0x00, /*          */
	0x00, /*          */

	/* @1104 'u' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x33, /*   ##  ## */
	0x3B, /*   ### ## */
	0x3E, /*   #####  */
	0x00, /*          */
	0x00, /*          */

	/* @1116 'v' (7 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x63, /*  ##   ## */
	0x36, /*   ## ##  */
	0x36, /*   ## ##  */
	0x36, /*   ## ##  */
	0x1C, /*    ###   */
	0x1C, /*    ###   */
	0x00, /*          */
	0x00, /*          */

	/* @1128 'w' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0xDB, /* ## ## ## */
	0xDB, /* ## ## ## */
	0xFE, /* #######  */
	0x7E, /*  ######  */
	0x7E, /*  ######  */
	0x66, /*  ##  ##  */
	0x00, /*          */
	0x00, /*          */

	/* @1140 'x' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x33, /*   ##  ## */
	0x1E, /*    ####  */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x1E, /*    ####  */
	0x33, /*   ##  ## */
	0x00, /*          */
	0x00, /*          */

	/* @1152 'y' (6 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x33, /*   ##  ## */
	0x1B, /*    ## ## */
	0x1E, /*    ####  */
	0x1E, /*    ####  */
	0x0E, /*     ###  */
	0x0C, /*     ##   */
	0x0C, /*     ##   */
	0x07, /*      ### */

	/* @1164 'z' (5 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x1F, /*    ##### */
	0x18, /*    ##    */
	0x0C, /*     ##   */
	0x06, /*      ##  */
	0x03, /*       ## */
	0x1F, /*    ##### */
	0x00, /*          */
	0x00, /*          */

	/* @1176 '{' (4 pixels wide) */
	0x00, /*          */
	0x0C, /*     ##   */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x03, /*       ## */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x0C, /*     ##   */

	/* @1188 '|' (2 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x03, /*       ## */
	0x00, /*          */
	0x00, /*          */

	/* @1200 '}' (4 pixels wide) */
	0x00, /*          */
	0x03, /*       ## */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x0C, /*     ##   */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x06, /*      ##  */
	0x03, /*       ## */

	/* @1212 '~' (8 pixels wide) */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0xCE, /* ##  ###  */
	0x7B, /*  #### ## */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
	0x00, /*          */
};

/* Character descriptors for Miriam 10pt */
/* { [Char width in bits], [Char height in bits], [Offset into miriam10ptCharBitmaps in bytes] } */
const FONT_CHAR_INFO miriam10ptDescriptors[] =
{
	{
		2, 12, 0 }, /*   */
	{
		2, 12, 12 }, /* ! */
	{
		4, 12, 24 }, /* " */
	{
		8, 12, 36 }, /* # */
	{
		7, 12, 48 }, /* $ */
	{
		9, 12, 60 }, /* % */
	{
		9, 12, 84 }, /* & */
	{
		2, 12, 108 }, /* ' */
	{
		3, 12, 120 }, /* ( */
	{
		3, 12, 132 }, /* ) */
	{
		6, 12, 144 }, /* * */
	{
		8, 12, 156 }, /* + */
	{
		3, 12, 168 }, /* , */
	{
		4, 12, 180 }, /* - */
	{
		2, 12, 192 }, /* . */
	{
		5, 12, 204 }, /* / */
	{
		5, 12, 216 }, /* 0 */
	{
		4, 12, 228 }, /* 1 */
	{
		5, 12, 240 }, /* 2 */
	{
		6, 12, 252 }, /* 3 */
	{
		6, 12, 264 }, /* 4 */
	{
		6, 12, 276 }, /* 5 */
	{
		5, 12, 288 }, /* 6 */
	{
		6, 12, 300 }, /* 7 */
	{
		5, 12, 312 }, /* 8 */
	{
		5, 12, 324 }, /* 9 */
	{
		2, 12, 336 }, /* : */
	{
		3, 12, 348 }, /* ; */
	{
		7, 12, 360 }, /* < */
	{
		7, 12, 372 }, /* = */
	{
		7, 12, 384 }, /* > */
	{
		5, 12, 396 }, /* ? */
	{
		11, 12, 408 }, /* @ */
	{
		9, 12, 432 }, /* A */
	{
		7, 12, 456 }, /* B */
	{
		7, 12, 468 }, /* C */
	{
		7, 12, 480 }, /* D */
	{
		7, 12, 492 }, /* E */
	{
		6, 12, 504 }, /* F */
	{
		8, 12, 516 }, /* G */
	{
		7, 12, 528 }, /* H */
	{
		2, 12, 540 }, /* I */
	{
		5, 12, 552 }, /* J */
	{
		8, 12, 564 }, /* K */
	{
		6, 12, 576 }, /* L */
	{
		9, 12, 588 }, /* M */
	{
		7, 12, 612 }, /* N */
	{
		8, 12, 624 }, /* O */
	{
		7, 12, 636 }, /* P */
	{
		8, 12, 648 }, /* Q */
	{
		7, 12, 660 }, /* R */
	{
		7, 12, 672 }, /* S */
	{
		6, 12, 684 }, /* T */
	{
		7, 12, 696 }, /* U */
	{
		8, 12, 708 }, /* V */
	{
		12, 12, 720 }, /* W */
	{
		8, 12, 744 }, /* X */
	{
		8, 12, 756 }, /* Y */
	{
		6, 12, 768 }, /* Z */
	{
		3, 12, 780 }, /* [ */
	{
		4, 12, 792 }, /* \ */
	{
		3, 12, 804 }, /* ] */
	{
		6, 12, 816 }, /* ^ */
	{
		8, 12, 828 }, /* _ */
	{
		3, 12, 840 }, /* ` */
	{
		6, 12, 852 }, /* a */
	{
		6, 12, 864 }, /* b */
	{
		6, 12, 876 }, /* c */
	{
		6, 12, 888 }, /* d */
	{
		6, 12, 900 }, /* e */
	{
		4, 12, 912 }, /* f */
	{
		6, 12, 924 }, /* g */
	{
		6, 12, 936 }, /* h */
	{
		2, 12, 948 }, /* i */
	{
		3, 12, 960 }, /* j */
	{
		6, 12, 972 }, /* k */
	{
		2, 12, 984 }, /* l */
	{
		10, 12, 996 }, /* m */
	{
		6, 12, 1020 }, /* n */
	{
		6, 12, 1032 }, /* o */
	{
		6, 12, 1044 }, /* p */
	{
		6, 12, 1056 }, /* q */
	{
		4, 12, 1068 }, /* r */
	{
		5, 12, 1080 }, /* s */
	{
		4, 12, 1092 }, /* t */
	{
		6, 12, 1104 }, /* u */
	{
		7, 12, 1116 }, /* v */
	{
		8, 12, 1128 }, /* w */
	{
		6, 12, 1140 }, /* x */
	{
		6, 12, 1152 }, /* y */
	{
		5, 12, 1164 }, /* z */
	{
		4, 12, 1176 }, /* { */
	{
		2, 12, 1188 }, /* | */
	{
		4, 12, 1200 }, /* } */
	{
		8, 12, 1212 }, /* ~ */
};

/* Font information for Miriam 10pt */
const FONT_INFO miriam10ptFontInfo =
{
	12, /*  Character height */
	' ', /*  Start character */
	'~', /*  End character */
	miriam10ptDescriptors, /*  Character decriptor array */
	miriam10ptBitmaps, /*  Character bitmap array */
};

