/*
 * spidev.cpp
 *
 *  Created on: Jul 21, 2017
 *      Author: dmk
 */
/*
 Connect the SPI Slave device to the following pins on the esp8266:
 GPIO    NodeMCU   Name  |   Uno
 ===================================
 13       D7      MOSI  |   D11
 12       D6      MISO  |   D12
 14       D5      SCK   |   D13
 */

#include <ESP8266WiFi.h>
#include <SPI.h>
#include "spidev.h"

void selectSpiDev(unsigned char cs_dev)
{
	switch(cs_dev)
	{
		default:
		case NONE_CS:
			digitalWrite(CS_DECODER_PIN1, LOW);
			digitalWrite(CS_DECODER_PIN2, LOW);
			break;
		case EXT2_CS:
			digitalWrite(CS_DECODER_PIN1, HIGH);
			digitalWrite(CS_DECODER_PIN2, LOW);
			break;
		case OLED_CS:
			digitalWrite(CS_DECODER_PIN1, LOW);
			digitalWrite(CS_DECODER_PIN2, HIGH);
			break;
		case PORT_CS:
			digitalWrite(CS_DECODER_PIN1, HIGH);
			digitalWrite(CS_DECODER_PIN2, HIGH);
			break;
	}
}
void initLbotSPI(void)
{
	pinMode(CS_DECODER_PIN1, OUTPUT);
	pinMode(CS_DECODER_PIN2, OUTPUT);

	SPI.begin();
	SPI.setClockDivider(SPI_CLOCK_DIV2);
	//0x00
	//0x11
	//0x10 default
//	SPI.setDataMode(0x10);

}

unsigned char transferSpiDev(unsigned char write_data, unsigned char mode)
{
	SPI.setDataMode(mode);
	return SPI.transfer(write_data);
}

