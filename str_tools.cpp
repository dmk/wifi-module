/*
 * str_tools.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: admin
 */
#include "Arduino.h"
#include <stdio.h>
#include "FS.h"
#include "board_conf.h"
#include "debugMacros.h"


void ipToUchar(int ipInt, char *str) {
	unsigned char *ip = (unsigned char*)&ipInt;
	sprintf(str,"%d.%d.%d.%d",ip[0],ip[1],ip[2],ip[3]);
}

#if 0
IPAddress ip = WiFi.localIP();
sprintf(lcdBuffer, "%d.%d.%d.%d:%d", ip[0], ip[1], ip[2], ip[3], udpPort);

 @Vijayenthiran
Vijayenthiran commented on Aug 17, 2017
Thanks. You can also use inbulilt .toString() funciton. Example usage:
server.send(200, "text/plain", "Please go to : " + WiFi.localIP().toString() + "/test");
#endif

#define DPRINTF_(...)       Serial.printf(__VA_ARGS__)

void printline(char *data, int N)
{
	int i;

	for(i=0;i<N;i++)
		DPRINTF_("%02x ", data[i]);
	DPRINTF_(" ");
	for(i=0;i<N;i++){
		if(isgraph((unsigned) data[i]))
		{
			DPRINTF_("%c", data[i]);
		}
		else
		{
			DPRINTF_(".");
		}
	}
	DPRINTF_("\n");
}
void printlineToFile(char *data, int N, File &f)
{
	int i;

	for(i=0;i<N;i++)
	{
		f.printf("%02x ", data[i]);
	}
	f.printf(" ");
	for(i=0;i<N;i++){
		if(isgraph((unsigned) data[i]))
		{
			f.printf("%c", data[i]);
		}
		else
		{
			f.printf(".");
		}
	}
	f.printf("\n");
}

void dumpbuff(char *buff, int N, File &f)
{
	int i;
	for(i=0;i<N;i+=16)
	{
		if(i + 16 <= N)
		{
			printlineToFile(buff+i, 16, f);
		}
		else
		{
			printlineToFile(buff+i, N - i, f);
		}
	}
}

void dumpbuff(char *buff, int N)
{
	int i;
	for(i=0;i<N;i+=16)
	{
		if(i + 16 <= N)
		{
			printline(buff+i, 16);
		}
		else
		{
			printline(buff+i, N - i);
		}

	}
}



