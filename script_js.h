/*
 * script_js.h
 *
 *  Created on: Feb 7, 2020
 *      Author: admin
 */

#ifndef SCRIPT_JS_H_
#define SCRIPT_JS_H_

const char* script = R"rawliteral(

function saveSSID() {
    var xhttp = new XMLHttpRequest();
    var ssid = document.getElementById('ssid');
    var passwd = document.getElementById('passw');
    if (ssid.value === '') {
        alert('SSID is missing');
    }else{
        xhttp.open('POST', '/savePass', true);
        xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhttp.send('ssid=' + ssid.value + '&passw=' + passwd.value);

        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                alert('SSID saved');
            }
        };
    }
}

function saveIP() {
    var xhttp = new XMLHttpRequest();
    var ip = document.getElementById('ip');
    var mask = document.getElementById('mask');
    var gateway = document.getElementById('gateway');
    var hostname = document.getElementById('hostname');
    var staticIp = document.getElementById('staticIp');
    var softAp = document.getElementById('softAp');
    xhttp.open('POST', '/save', true);
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	var staticIpstring = 'off';
	if( staticIp.checked){
		staticIpstring = 'on';
	}
	var softApstring = 'off';
	if( softAp.checked){
		softApstring = 'on';
	}
    xhttp.send('ip=' + ip.value + '&mask=' + mask.value + '&gateway=' + gateway.value + '&hostname=' + hostname.value + '&staticIp=' + staticIpstring + '&softAp=' + softApstring);
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            alert('IP saved');
        }
    };
}
function reboot() {
    var xhttp = new XMLHttpRequest();

    xhttp.open('POST', '/reboot', true);
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhttp.send();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            alert('reboot');
        }
    };
}

)rawliteral";


#endif /* SCRIPT_JS_H_ */
