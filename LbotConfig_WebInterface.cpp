/*
 * LbotConfig_WebInterface.cpp
 *
 *  Created on: Mar 6, 2018
 *      Author: dkunin
 */
#include "board_conf.h"
#include "LbotConfig.h"
#include "LbotSensorReading.h"
#include "IoT_HostInteface.h"
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

//#include <ESPAsyncTCP.h>
//#include <ESPAsyncWebServer.h>
#include <FS.h>

#include <ESP8266mDNS.h>
//#include "jquery_js.h"
#include "script_js.h"
#include "style_css.h"
#include "debugMacros.h"

//#include "ArduinoJson.h"

String  statLed = "OFF";
//boolean simpleStarted = false;
boolean regularStarted = false;

const char* upgradePage = "<form method='POST' action='/update' "\
		"enctype='multipart/form-data'><input type='file' name='update'>"\
		"<input type='submit' value='Update'></form>";

extern LbotConfig lbotconfig;
extern LbotSensorReading reading;
extern int Serial_mutex;
extern String getTCP_ServerStatus();

//StaticJsonBuffer<200> jsonBuffer;

// create Objects
//DHT dht(DHTPIN, DHTTYPE);
//Adafruit_BMP085 bmp;
ESP8266WebServer *server = new ESP8266WebServer(80);

void save_passw_ssid(String qsid, String qpass);

void encriptPassw(String &pass)
{
	int i;
	for(i = 0; (pass.charAt(i) != '\0'); i++)
		pass.setCharAt(i, pass.charAt(i) + 2); //the key for encryption is 3 that is added to ASCII value
}
void decriptPassw(String &pass)
{
	int i;
	for(i = 0; (pass.charAt(i) != '\0'); i++)
		pass.setCharAt(i, pass.charAt(i) - 2); //the key for encryption is 3 that is subtracted to ASCII value
}


String getPageHead(int menu_active){
	String header = "<head>";


	if (3 == menu_active)
		header += "<meta http-equiv='refresh' content='3' name='viewport' content='width=device-width, initial-scale=1.0'>";
	else
		header += "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
	header += "<link rel='stylesheet' href='/lbotstyle.css'>";
	header += "<script type='text/javascript' src='/lbotscript.js'></script>";
//	header += "<script type='text/javascript' src='/jquery.js'></script>";
	header += "<title>";
	header += DEVICE_NAME;
	header += "</title>";
	header += "</head>";
	return header;
}

String getPageBody1(String content, int menu_active){

	String body = "<body>";
	body +="<div class=\"grid-container\">";
	body +="<div class=\"item1\">";
	body +="<ul>";
	if (0 == menu_active)
		body +="<li><a class=\"active\" href=\"/\">Main</a></li>";
	else
		body +="<li><a href=\"/\">Main</a></li>";

	if (1 == menu_active)
		body +="<li><a class=\"active\" href=\"setup\">Setup</a></li>";
	else
		body +="<li><a href=\"setup\">Setup</a></li>";

	if (2 == menu_active)
		body +="<li><a class=\"active\" href=\"update\">Update</a></li>";
	else
		body +="<li><a href=\"update\">Update</a></li>";

	if (3 == menu_active)
		body +="<li><a class=\"active\" href=\"about\">About</a></li>";
	else
		body +="<li><a href=\"about\">About</a></li>";

	body +="</ul>";

	body +="</div>";
	body +="<div class=\"item2\">";
	body += content;
	body +="</div>";
	body +="</div>";

	body += "<div class='footer'><p>";
	body += DEVICE_MODEL;
	body += "&copy; 2019 (Build ";
	body += DEVICE_VERSION;
	body += ", ";
    body += __DATE__;
    body += " ";
    body += __TIME__;
	body += ")</p></div>";

	body +="</body>";
	return body;
}

String getMainContent(String hostname){
	String content = "<span class='mdl-layout-title lbot-title-1'>Main page</span>";
	content += "<div class='mdl-grid'>";
	content += "<div class='mdl-cell mdl-cell--6-col'>";
	content += "Read manual before you set up device <b>";
	content += hostname;
	content += "</b><br>";
	content += "</div>";
	content += "</div>";
	return content;
}

String getSetupContent(String ssid, String passw, boolean staticIp, String ip, String mask, String gateway, String hostname, boolean softAp){
	String setup = "<span class='mdl-layout-title lbot-title-1'>SETUP</span>";
	setup += "<span class='lbot-title-2'>NETWORK</span>";
	setup += "<form id='savePassForm'>";
	setup += "<div class='mdl-grid'>";
	setup += "<div class='mdl-cell mdl-cell--6-col'>";
	setup += "<div class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>";
	setup += "<input class='mdl-textfield__input' type='text' id='ssid' name='ssid' value='";
	setup += ssid;
	setup += "'>";
	setup += "<label class='mdl-textfield__label' for='ssid'>SSID</label>";
	setup += "</div>";
	setup += "</div>";
	setup += "<div class='mdl-cell mdl-cell--6-col'>";
	setup += "<div class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>";
	setup += "<input class='mdl-textfield__input' type='password' id='passw' name='passw'  value='";
	encriptPassw(passw);
	setup += passw;
	setup += "'>";
	setup += "<label class='mdl-textfield__label' for='passw'>Password</label>";
	setup += "</div>";
	setup += "</div>";
	setup += "<div class='mdl-cell mdl-cell--12-col'>";
    setup += "<button onClick='saveSSID()' class='button' name='savePass' id='savePass'>SAVE</button>";
//	setup += "<button class='mdl-button mdl-js-button mdl-button--raised mdl-button--colored' name='savePass' id='savePass'>SAVE</button>";
	setup += "</div>";


	setup += "</div>";
	setup += "</form>";
	setup += "<hr>";
	setup += "<form id='saveForm'>";
	setup += "<div class='mdl-grid'>";

//    <input type='checkbox' id='softAp' name='softAp' /><label for='softAp'></label><span>WiFi Access Point</span>
//    <input type='checkbox' id='staticIp' name='staticIp' checked /><label for='staticIp'></label><span>Static IP</span>




//	setup += "<div class='mdl-cell mdl-cell--6-col'>";
//	setup += "<div class='coolCheckbox'>";
	setup += "<input type='checkbox' id='softAp' name='softAp' ";
	if(softAp) {
		setup += " checked";
	}
	setup += "/>";
	setup += "<label for='softAp'></label>";
	setup += "<span>WiFi Access Point</span>";
//	setup += "</div>";
//	setup += "</div>";

//	setup += "<div class='mdl-cell mdl-cell--6-col'>";
//	setup += "<div class='coolCheckbox'>";
	setup += "<input type='checkbox' id='staticIp' name='staticIp' ";
	if(staticIp) {
		setup += " checked";
	}
	setup += "/>";
	setup += "<label for='staticIp'></label>";
	setup += "<span>Static IP</span>";
//	setup += "</div>";
//	setup += "</div>";

	setup += "<div class='mdl-cell mdl-cell--6-col'>";
	setup += "<div class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>";
	setup += "<input class='mdl-textfield__input' type='text' id='ip' pattern='(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' name='ip' value='";
	setup += ip;
	setup += "'>";
	setup += "<label class='mdl-textfield__label' for='ip'>IP</label>";
	setup += "</div>";
	setup += "</div>";

	setup += "<div class='mdl-cell mdl-cell--6-col'>";
	setup += "<div class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>";
	setup += "<input class='mdl-textfield__input' type='text' id='mask' pattern='(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' name='mask' value='";
	setup += mask;
	setup += "'>";
	setup += "<label class='mdl-textfield__label' for='mask'>Subnet Mask</label>";
	setup += "</div>";
	setup += "</div>";


	setup += "<div class='mdl-cell mdl-cell--6-col'>";
	setup += "<div class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>";
	setup += "<input class='mdl-textfield__input' type='text' id='gateway' pattern='(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' name='gateway' value='";
	setup += gateway;
	setup += "'>";
	setup += "<label class='mdl-textfield__label' for='gateway'>Gateway</label>";
	setup += "</div>";
	setup += "</div>";

	setup += "<div class='mdl-cell mdl-cell--6-col'>";
	setup += "<div class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>";
	setup += "<input class='mdl-textfield__input' type='text' id='hostname' name='hostname' value='";
	setup += hostname;
	setup += "'>";
	setup += "<label class='mdl-textfield__label' for='hostname'>Hostname</label>";
	setup += "</div>";
	setup += "</div>";
	setup += "</div>";
	setup += "</form>";

	setup += "<div class='mdl-grid'>";
	setup += "<div class='mdl-cell mdl-cell--2-col'>";
	setup += "<button onClick='saveIP()' class='button' name='save' id='save'>SAVE</button>";
	setup += "</div>";
	setup += "<div class='mdl-cell mdl-cell--10-col'>";
	setup += "<button onClick='reboot()' class='button' name='reboot' id='reboot'>REBOOT</button>";
	setup += "</div>";
	setup += "</div>";

	//	setup += "<div class='mdl-grid'>";
	//	setup += "<div class='mdl-cell mdl-cell--2-col'>";
	//	setup += "<button class='mdl-button mdl-js-button mdl-button--raised mdl-button--accent' name='update' id='update'>UPDATE</button>";
	//	setup += "</div>";

	return setup;
}

String getAboutContent(String debug_string) {
	String content = "<span class='mdl-layout-title lbot-title-1'>About</span>";
	content +="<div style=\"margin-left:25%;padding:1px 16px;height:1000px;\">";
	content +="<p>Memad Medical Ltd, WiFi module for VL667.</p>";
	content +=debug_string;
	content +="</div>";
	return content;
}
const char* serverIndex = "<form method='POST' action='/update' enctype='multipart/form-data'><input type='file' name='update'><input type='submit' value='Update'></form>";

String getSimpleUpdateContent() {
	String content = "<p title=\"Wifi VL-667 fw update\">UPDATE</p>";
	content +="<div style=\"margin-left:25%;padding:1px 16px;height:1000px;\">";
	content +="<p>Select firmware image file, and press Update for for update WiFi_VL667.</p>";
	content +="<form method='POST' action='/runUpdate' enctype='multipart/form-data'><input type='file' name='runUpdate'><input type='submit' value='Update'></form>";
	content +="</div>";
	return content;
}
String getUpdateContent() {
	String content = "<span class='mdl-layout-title lbot-title-1'>UPDATE</span>";
	content += "<div class='mdl-grid'>";
	content += "<div class='mdl-cell mdl-cell--12-col'>";

	content += "<form  method='POST' enctype='multipart/form-data' id='updateForm'>";
	content += "<div class='mdl-textfield mdl-js-textfield mdl-textfield--file'>";
	content += "<input class='mdl-textfield__input' placeholder='File' type='text' id='uploadFile' readonly/>";
	content += "<div class='mdl-button mdl-button--primary mdl-button--icon mdl-button--file'>";
	content += "<i class='material-icons'>attach_file</i><input type='file' accept='.bin' id='uploadBtn'>";
	//	content += "<svg class='svg-icon' viewBox='0 0 20 20'><path d='M4.317,16.411c-1.423-1.423-1.423-3.737,0-5.16l8.075-7.984c0.994-0.996,2.613-0.996,3.611,0.001C17,4.264,17,5.884,16.004,6.88l-8.075,7.984c-0.568,0.568-1.493,0.569-2.063-0.001c-0.569-0.569-0.569-1.495,0-2.064L9.93,8.828c0.145-0.141,0.376-0.139,0.517,0.005c0.141,0.144,0.139,0.375-0.006,0.516l-4.062,3.968c-0.282,0.282-0.282,0.745,0.003,1.03c0.285,0.284,0.747,0.284,1.032,0l8.074-7.985c0.711-0.71,0.711-1.868-0.002-2.579c-0.711-0.712-1.867-0.712-2.58,0l-8.074,7.984c-1.137,1.137-1.137,2.988,0.001,4.127c1.14,1.14,2.989,1.14,4.129,0l6.989-6.896c0.143-0.142,0.375-0.14,0.516,0.003c0.143,0.143,0.141,0.374-0.002,0.516l-6.988,6.895C8.054,17.836,5.743,17.836,4.317,16.411'></path></svg>"
	//	content += "<input type='file' accept='.bin' id='uploadBtn'>";
	content += "</form>";
	content += "</div>";
	content += "</div>";

	content += "</div>";
	content += "<div class='mdl-cell mdl-cell--12-col'>";
	content += "<span style='color: red' class='lbot-title-2'>PROCEED WITH CAUTION!</span>";
	content += "</div>";

	content += "<div class='mdl-cell mdl-cell--12-col'>";
	content += "<button class='mdl-button mdl-js-button mdl-button--raised mdl-button--colored' name='runUpdate' id='runUpdate'>UPDATE</button>";
	content += "</div>";
	content += "</div>";
	return content;
}



String getMainPage(String hostname){
	String page = "<!DOCTYPE html><html>";
	page += getPageHead(0);
	String content = getMainContent(hostname);
	page += getPageBody1(content,0);
	page += "</html>";
	return page;
}

String getSetupPage(String ssid, String passw, boolean staticIp, String ip, String mask, String gateway, String hostname, boolean softAp){
	String page = "<!DOCTYPE html><html>";
	page += getPageHead(1);
	String content = getSetupContent(ssid, passw, staticIp, ip, mask, gateway, hostname, softAp);
	page += getPageBody1(content,1);
	page += "</html>";
	return page;
}

String getUpdatePage(){
	String page = "<!DOCTYPE html><html>";
	page += getPageHead(2);

//	String content = getUpdateContent();
	String content = getSimpleUpdateContent();

	page += getPageBody1(content,2);
	page += "</html>";
	return page;
}

String getAboutPage(String debug_string){
	String page = "<!DOCTYPE html><html>";
	page += getPageHead(3);
	String content = getAboutContent(debug_string);
	page += getPageBody1(content,3);
	page += "</html>";
	return page;
}

// end Menu Pages


void handleStyle() {
	DPRINTLN("handle style");
	server->send ( 200, "text/css", style);// style
}

void handleScript() {
	DPRINTLN("handle script");
	server->send ( 200, "text/javascript", script);// script
}

void handleLog() {
	DPRINTLN("handle log");
	char buf[16] = {0};
	strcpy(buf,"ABCD");
//	String logString = tempLog.readString();
//	DPRINTLN(logString.c_str());
#ifdef LOG_ENA
	File tempLog = SPIFFS.open("/temp.log", "r");
	size_t fileSize = tempLog.size();
#endif
//	server->sendHeader("Content-Length", (String)fileSize);
	server->sendHeader("Content-Length", String(5));
	server->send( 200, "text/plain", "");// script

	server->sendContent(buf); //send the current buffer
#ifdef LOG_ENA

	char buffer[64];
	while (tempLog.available()) {
	 int l = tempLog.readBytesUntil('\n', buffer, sizeof(buffer));
	 buffer[l] = 0;
//	 if(l>0)
//		 server->sendContent_P(buffer, l); //send the current buffer
	 DPRINT("Data len ");
	 DPRINT(l);
	 DPRINT(" ");
	 DPRINTLN(buffer);
	}
#endif
	server->sendContent(""); //send the current buffer
#ifdef LOG_ENA
	tempLog.close();
#endif
}


void handleRoot(){
	String hostname = lbotconfig.getHostName();
	server->send ( 200, "text/html", getMainPage(hostname));// main page
}

void handleReboot () {
	String message = lbotconfig.getHostName();
	message += " is rebooting!";
	server->send ( 200, "text/html", message);
	ESP.restart();
}


void handleUpdate () {
	server->send ( 200, "text/html", getUpdatePage());
}
//extern int Serial_wr_cntr;
//extern int Serial_rd_cntr;
//extern int Serial_timeout_cntr;
//extern int serverState[];
//extern String getConnectionStateStr(int8_t);

void handleAbout () {
//	String debug_string = "TCP Server STATUS: \n";
//
//	debug_string += "WR="+String(Serial_wr_cntr)+" RD="+String(Serial_rd_cntr)+" TO="+String(Serial_timeout_cntr);
//	debug_string += " Connection 1 state=";
//	debug_string += getConnectionStateStr(0);
//	debug_string += " Connection 2 state=";
//	debug_string += getConnectionStateStr(1);






	String debug_string = getTCP_ServerStatus();

	server->send ( 200, "text/html", getAboutPage(debug_string));
}

void handleRunUpdate() {
	Serial_mutex = 1;
	HTTPUpload& upload = server->upload();
	if (upload.status == UPLOAD_FILE_START) {
		Serial.setDebugOutput(true);
		WiFiUDP::stopAll();
		if(upload.filename != NULL)	{
			DPRINTF("Update: %s\n", upload.filename.c_str());
			uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
			if (!Update.begin(maxSketchSpace)) { //start with max available size
				Update.printError(Serial);
				server->send(400, "text/html", "File is too big!");
			}
//			else {
//				server->send(200, "text/html", "Updating");
//			}
		} else {
			server->send(400, "text/html", "File name is misssing!");
		}
		DPRINT("Loadig file...");
	} else if (upload.status == UPLOAD_FILE_WRITE) {
		DPRINT(".");
		if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
			Update.printError(Serial);
		}
	} else if (upload.status == UPLOAD_FILE_END) {
		if (Update.end(true)) { //true to set the size to the current progress
			DPRINTF("Update Success: %u\nRebooting...\n", upload.totalSize);
			server->send(200, "text/html", "Update Successes\nRebooting...\n");
		} else {
			Update.printError(Serial);
		}
		Serial.setDebugOutput(false);
	}
	yield();
}

void handleRunUpdate_() {
	Serial_mutex = 1;
	HTTPUpload& upload = server->upload();
	if(upload.status == UPLOAD_FILE_START){
		Serial.setDebugOutput(true);
		WiFiUDP::stopAll();
		if(upload.filename != NULL)	{
			DPRINTF("Update: %s\n", upload.filename.c_str());
			uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
			if(!Update.begin(maxSketchSpace)){	//start with max available size
				Update.printError(Serial);
				server->send(400, "text/html", "File is too big!");
			}
			else {
				server->send(200, "text/html", "Updating");
			}
		} else {
			server->send(400, "text/html", "File name is misssing!");
		}
		DPRINT("Loadig file...");
	} else if(upload.status == UPLOAD_FILE_WRITE) {
		DPRINT(".");
		if(Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
			Update.printError(Serial);
		}
	} else if(upload.status == UPLOAD_FILE_END) {
		DPRINTF("-");
		if(Update.end(true)) { //true to set the size to the current progress
			delay(500);
			DPRINTLN();
			DPRINTF("Update Success: %u\nRebooting...\n", upload.totalSize);
			ESP.restart();

		} else {
			Update.printError(Serial);
		}
	} else {
		server->send(400, "text/html", "Upload didn't start");
	}
	DPRINTF("*");

	Serial.setDebugOutput(false);
	yield();
}
/*
server->on("/update", HTTP_POST, [](){
	server->sendHeader("Connection", "close");
	server->sendHeader("Access-Control-Allow-Origin", "*");
	server->send(200, "text/plain", (Update.hasError())?"FAIL":"OK");
	ESP.restart();
},[](){
	HTTPUpload& upload = server->upload();
	if(upload.status == UPLOAD_FILE_START){
		Serial.setDebugOutput(true);
		WiFiUDP::stopAll();
		Serial.printf("Update: %s\n", upload.filename.c_str());
		uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
		if(!Update.begin(maxSketchSpace)){//start with max available size
			Update.printError(Serial);
		}
	} else if(upload.status == UPLOAD_FILE_WRITE){
		if(Update.write(upload.buf, upload.currentSize) != upload.currentSize){
			Update.printError(Serial);
		}
	} else if(upload.status == UPLOAD_FILE_END){
		if(Update.end(true)){ //true to set the size to the current progress
			Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
		} else {
			Update.printError(Serial);
		}
		Serial.setDebugOutput(false);
	}
	yield();
});

*/


void handleSave () {
	String newHostname = server->arg("hostname");

	DPRINTLN("Soft Ap");
	DPRINTLN(server->arg("softAp"));
	boolean newSoftAp = server->arg("softAp") == "on" ? true : false;
	DPRINTLN("Setup:");
	DPRINT("hostname: ");
	DPRINTLN(newHostname);
	DPRINT("softAP: ");
	DPRINTLN(newSoftAp);

	lbotconfig.setHostName((char*)newHostname.c_str(),newHostname.length());
	lbotconfig.setSoftAp(newSoftAp);

	boolean staticIp = server->arg("staticIp") == "on" ? true : false;
	DPRINT("staticIp: ");
	DPRINTLN(staticIp);

	lbotconfig.setStaticIpEnable(staticIp);

	IPAddress ip;
	ip.fromString(server->arg("ip"));
	lbotconfig.setIp(ip);
	DPRINTLN(ip);

	IPAddress gateway;
	gateway.fromString(server->arg("gateway"));
	lbotconfig.setGate(gateway);
	DPRINTLN(gateway);

	IPAddress mask;
	mask.fromString(server->arg("mask"));
	lbotconfig.setMask(mask);
	DPRINTLN(mask);

	lbotconfig.save();

#if VERIFY_EEPROM
	lbotconfig.read();
	lbotconfig.isSoftAP();
	lbotconfig.getHostName();
#endif
	//temporary restore default password and SSID
	lbotconfig.begin();
	server->send (200, "text/html", "OK");
	//		server->send (400, "text/html", "Missing Parameters"); // for filed validation
}

void handleNotFound () {
	server->send(404, "text/html", "<html><body style='margin: auto; width 80%'><b>404</b><br>LBoT<br>Page Not found</body></html>"); // HTTP status 404 - Not Found
}

void handleSavePass () {
	if (server->hasArg("ssid")) {
		String newSsid = server->arg("ssid");
		String newPassw = server->arg("passw");
		String hostName = server->arg("hostName");

		DPRINTF("Set WiFi access credentials: '%s' / '%s'", newSsid.c_str(), newPassw.c_str());
		lbotconfig.setSsid((char*)newSsid.c_str(),newSsid.length());
		decriptPassw(newPassw);
		lbotconfig.setPassw((char*)newPassw.c_str(),newPassw.length());
		if(hostName.length() > 0) {
    		lbotconfig.setHostName((char*)hostName.c_str(),hostName.length());
		}
		lbotconfig.save();

#if VERIFY_EEPROM
		lbotconfig.read();
		lbotconfig.getSsid();
		lbotconfig.getPassw();
		Serial.print("New SSID "); Serial.println(lbotconfig.getSsid());
		Serial.print("New PASSW "); Serial.println(lbotconfig.getPassw());
#endif
		//temporary restore default password and SSID
		lbotconfig.begin();
		server->send (200, "text/html", "OK");
		if(hostName.length() > 0) { // TODO this is temporary measure. When all pages will be network independent we can remove it [agk].
			ESP.restart();
		}
	} else {
		server->send (400, "text/html", "Missing Parameters");
	}
}

void handleSetup () {
	String ssid = lbotconfig.getSsid();
	String passw = lbotconfig.getPassw();
	String hostname = lbotconfig.getHostName();
	boolean softAp = lbotconfig.isSoftAP();

	boolean staticIp = lbotconfig.isStaticIpEnable();
	IPAddress ipInt = lbotconfig.getIp();
	String ip = ipInt.toString();

	IPAddress gateInt = lbotconfig.getGate();
	String gateway = gateInt.toString();

	IPAddress maskInt = lbotconfig.getMask();
	String mask = maskInt.toString();

	server->send ( 200, "text/html", getSetupPage(ssid, passw, staticIp, ip, mask, gateway, hostname, softAp));// main page
}


//Use this to send an Image or File from SPIFFS
void loadImg(String path, String TyPe)
{ // path=Filename(including root "/"), TyPe= html descriptor
 if(SPIFFS.exists(path)){
    File file = SPIFFS.open(path, "r");
    server->streamFile(file, TyPe);
    file.close();
  }
}





void initWebServer () {
	// link to the function that manage launch page
	DPRINTLN("Setting up app on WEB server");
	server->on("/", handleRoot);
	server->on("/lbotstyle.css", handleStyle);
//	server->on("/jquery.js", handleJqueryScript);
	server->on("/lbotscript.js", handleScript);
//	server->on("/temp.log", handleLog);

//  Any files need to be uploaded to SPIFFS...!!!
#ifdef LOG_ENA
	 server->on("/temp.log", []() { loadImg("/temp.log", "text/plain"); } );  // or jpg
#endif

	server->on("/setup", handleSetup);
//	server->on("/mqttSetup", handleMqttSetup);
//	server->on("/readings", handleReadings);
//	server->on("/refreshReadings", handleRefreshReadings);
	server->on("/savePass", handleSavePass);
	server->on("/save", handleSave);
//	server->on("/mqttSave", handleMqttSave);
	server->on("/about", handleAbout);
	server->on("/reboot", handleReboot);
	server->on("/update", handleUpdate);
//	server->on("/runUpdate", HTTP_POST, [](){
//			server->send(200, "text/html", "OK");
//			server->sendHeader("Connection", "close");
//			server->send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
//			ESP.restart();
//
//			},
//			handleRunUpdate);


	server->on("/runUpdate", HTTP_POST, []() {
	   server->sendHeader("Connection", "close");
	   server->sendHeader("Access-Control-Allow-Origin", "*");
	   server->send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");

//	    server->sendHeader("Location", "/",true); //Redirect to our html web page
//	    server->send(302, "text/plane","");
	   ESP.restart();
	 },
	 handleRunUpdate);

	server->begin();
	MDNS.addService("http", "tcp", 80);
}

void stopServer() {
	DPRINTLN("Stop WEB server...");
	delete server;
	server = new ESP8266WebServer(80);
}
boolean IsWebStarted()
{
	return regularStarted;
}

void setupWeb() {
	if(!regularStarted) {
		stopServer();
		initWebServer();
		regularStarted = true;
	}
}

void loopWeb() {
	static int pollingPeriod;

	if(pollingPeriod > 100) {
		pollingPeriod = 0;
		server->handleClient();
	}
	pollingPeriod++;
}



#if 0
/*
  To upload through terminal you can use: curl -F "image=@firmware.bin" esp8266-webupdate.local/update
 */

#define AP_STA

//const char* host = "lbot";
#ifdef AP_STA
//const char* ssid = "fwe251";
//const char* password = "";
#else
const char* ssid = "LBOT";
const char* password = "lbot";
IPAddress local_IP(192,168,59,10);
IPAddress gateway(192,168,59,3);
IPAddress subnet(255,255,255,0);
#endif


void setup_web_upd(void){
	Serial.begin(115200);
	Serial.println();
	Serial.println("Booting Sketch...");
#ifdef AP_STA
	WiFi.mode(WIFI_AP_STA);
	// WiFi.softAP(ssid);
	// WiFi.begin(ssid, password);
	while(WiFi.waitForConnectResult() != WL_CONNECTED){
		delay(500);
		Serial.print(".");
		//ESP.restart();
	}
	if(WiFi.waitForConnectResult() == WL_CONNECTED){
#else
		WiFi.mode(WIFI_AP);
		WiFi.softAPConfig(local_IP, gateway, subnet);
		WiFi.softAP(ssid, password);

		if(1){
			delay(2000);
#endif
			//    MDNS.begin(host);
			server->on("/", HTTP_GET, [](){
				server->sendHeader("Connection", "close");
				server->sendHeader("Access-Control-Allow-Origin", "*");
				server->send(200, "text/html", upgradePage);
			});
			server->on("/update", HTTP_POST, [](){
				server->sendHeader("Connection", "close");
				server->sendHeader("Access-Control-Allow-Origin", "*");
				server->send(200, "text/plain", (Update.hasError())?"FAIL":"OK");
				ESP.restart();
			},[](){
				HTTPUpload& upload = server->upload();
				if(upload.status == UPLOAD_FILE_START){
					Serial.setDebugOutput(true);
					WiFiUDP::stopAll();
					Serial.printf("Update: %s\n", upload.filename.c_str());
					uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
					if(!Update.begin(maxSketchSpace)){//start with max available size
						Update.printError(Serial);
					}
				} else if(upload.status == UPLOAD_FILE_WRITE){
					if(Update.write(upload.buf, upload.currentSize) != upload.currentSize){
						Update.printError(Serial);
					}
				} else if(upload.status == UPLOAD_FILE_END){
					if(Update.end(true)){ //true to set the size to the current progress
						Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
					} else {
						Update.printError(Serial);
					}
					Serial.setDebugOutput(false);
				}
				yield();
			});
			server->begin();
			MDNS.addService("http", "tcp", 80);
			Serial.print("Connected, IP address: ");
			Serial.println(WiFi.localIP());
			//    Serial.printf("Ready! Open http://%s.local in your browser\n", host);
		} else {
			Serial.println("WiFi Failed");
		}
	}



	//void loop_web_upd(void){
	//  server->handleClient();
	//  delay(1);
	//}


#endif
