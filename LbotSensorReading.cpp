/*
 * LbotSensorReading.h
 *
 *  Created on: 4/23/2018
 *      Author: akunin
 */

#include <string.h>
#include "LbotSensorReading.h"

LbotSensorReading::LbotSensorReading() {
}

void LbotSensorReading::setTemperature(double temperature){
	m_sensor_reading.tempSet = true;
	m_sensor_reading.temp = temperature;
}

void LbotSensorReading::setTemperatureH(double temperatureHum){
	m_sensor_reading.tempHumSet = true;
	m_sensor_reading.tempHum = temperatureHum;
}

void LbotSensorReading::setHumidity(double humidity){
	m_sensor_reading.humSet = true;
	m_sensor_reading.hum = humidity;
}

void LbotSensorReading::setTemperatureP(double temperaturePress){
	m_sensor_reading.tempPressSet = true;
	m_sensor_reading.tempPress = temperaturePress;
}

void LbotSensorReading::setPressure(double press){
	m_sensor_reading.pressSet = true;
	m_sensor_reading.press = press;
}

void LbotSensorReading::setRelayState(byte relayState){
	m_sensor_reading.relayState = relayState;
}
