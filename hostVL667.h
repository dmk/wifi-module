/*
 * hostVL667.h
 *
 *  Created on: Dec 23, 2019
 *      Author: admin
 */

#ifndef HOSTVL667_H_
#define HOSTVL667_H_


typedef enum
{
	CMD_READ = 0,
	CMD_WRITE = 0x40
}PROTOCOL_CMD_t;

int updateHost(int enforceFlg);
int waitForHostToStart();


class host_VL667
{
public:
	host_VL667();
	void update();
	void send_connection_parameters();
	void readCommandRegister();
};

#endif /* HOSTVL667_H_ */
