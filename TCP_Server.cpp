/*
 * TCP_Server.cpp
 *
 *  Created on: Apr 12, 2018
 *      Author: dkunin
 */
#include "board_conf.h"
#include "TCP_Server.h"
#include "debugMacros.h"
#include "str_tools.h"

//#include <ESP8266.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>



#define MAX_SRV_CLIENTS 2
#define IDLE_SERVER_STATE	0
#define REC_SERVER_STATE	1
#define GETRESP_SERVER_STATE	2
#define SEND_SERVER_STATE	3
#define TIMEOUT_30mS	30
#define TIMEOUT_40mS	40

const char *ServerStateStr[] = {
	"IDLE",
	"RECEIVE",
	"GET RESP",
	"SEND"
};

// Start a TCP Server on port 5009
WiFiServer TCPserver(5009);
WiFiClient TCPserverClients[MAX_SRV_CLIENTS];
int Serial_mutex = 0;
int Serial_wr_cntr[2] = {0,0};
int Serial_rd_cntr[2] = {0,0};
int Serial_timeout_cntr[2] = {0,0};
int Serial_lostSock_cntr[2] = {0,0};
int Serial_reject_cntr[2] = {0,0};
int Serial_opened_cntr[2] = {0,0};
int Polling_cntr[2] = {0,0};
int serverState[2] = {IDLE_SERVER_STATE,IDLE_SERVER_STATE};
int update_host = 0;

unsigned long previousMillis = 0;
const long interval = 200;

String getConnectionStateStr(int8_t connection_state)
{
	return String(ServerStateStr[connection_state]);
}

String getTCP_ServerStatus()
{
	String debug_string;
	debug_string = "<table>";
	debug_string +="<caption>TCP Server STATUS v3</caption>";

	debug_string +="<thead><th>Param&nbsp&nbsp</th><th>connection 1 &nbsp</th><th>connection 2</th></thead>";
	debug_string +="<tbody>";

	debug_string +="<tr>";
	debug_string +="<td>WR cycles</td><td>"+String(Serial_wr_cntr[0])+"&nbsp</td><td>"+String(Serial_wr_cntr[1])+"</td>";
	debug_string +="</tr>";

	debug_string +="<tr>";
	debug_string +="<td>RD cycles</td><td>"+String(Serial_rd_cntr[0])+"&nbsp</td><td>"+String(Serial_rd_cntr[1])+"</td>";
	debug_string +="</tr>";

	debug_string +="<tr>";
	debug_string +="<td>TO</td><td>"+String(Serial_timeout_cntr[0])+"&nbsp</td><td>"+String(Serial_timeout_cntr[1])+"</td>";
	debug_string +="</tr>";

	debug_string +="<tr>";
	debug_string +="<td>Lost sock</td><td>"+String(Serial_lostSock_cntr[0])+"&nbsp</td><td>"+String(Serial_lostSock_cntr[1])+"</td>";
	debug_string +="<tr>";

	debug_string +="<tr>";
	debug_string +="<td>State</td><td>"+getConnectionStateStr(serverState[0])+"&nbsp</td><td>"+getConnectionStateStr(serverState[1])+"</td>";
	debug_string +="</tr>";

	debug_string +="<tr>";
	debug_string +="<td>Rejects</td><td>"+String(Serial_reject_cntr[0])+"&nbsp</td><td>"+String(Serial_reject_cntr[1])+"</td>";
	debug_string +="</tr>";

	debug_string +="<tr>";
	debug_string +="<td>Opened</td><td>"+String(Serial_opened_cntr[0])+"&nbsp</td><td>"+String(Serial_opened_cntr[1])+"</td>";
	debug_string +="</tr>";

	debug_string +="</tbody>";
	debug_string += "</table>";
	return debug_string;
}




void startTcpServer()
{
#ifdef PROTOCOL_CONVERTER
	  // Start the TCP server
	TCPserver.begin();
	TCPserver.setNoDelay(true);
//    DPRINT("TCP server started\r\n");

#endif // PROTOCOL_CONVERTER
}

static uint8_t rbuf[2][240];
static uint8_t rbuf_len[2];
static uint8_t sbuf[2][240];
void loop_tcp_server() {
#ifdef PROTOCOL_CONVERTER

	static int polling_period;
	uint8_t i;
	size_t len;
	static unsigned long lastSerReceiveTime;
//	static int serverState[2] = {REC_SERVER_STATE};
	static int8_t connection = 0;
//	static uint8_t sbuf[2][240];
	static uint8_t sbufIndex[2];
	static uint8_t retryCntr[2];
	long timeout = 200L;


	if (polling_period > 10) {
		polling_period = 0;

		//check if there are any new clients
		if (TCPserver.hasClient()) {
			for (i = 0; i < MAX_SRV_CLIENTS; i++) {
				//find free/disconnected spot
				if (!TCPserverClients[i] || !TCPserverClients[i].connected()) {
					if (TCPserverClients[i]) {
						TCPserverClients[i].stop();
						Serial_opened_cntr[i]--;
						DPRINTF("Connection %d stopped ", i);
					}
					TCPserverClients[i] = TCPserver.available();
					Serial_opened_cntr[i]++;
					DPRINTF("New client: %d\n",i);
					serverState[i] = REC_SERVER_STATE;
					DPRINTF("Current connection %d Serverstate %s\n",connection,ServerStateStr[serverState[connection]]);
					break;
				}
			}
			//no free/disconnected spot so reject
			if (i == MAX_SRV_CLIENTS) {
				WiFiClient TCPserverClients = TCPserver.available();
				TCPserverClients.stop();
				DPRINTLN("Connection rejected ");
			}
		}
	}
	polling_period++;
	Polling_cntr[connection]++;
#if 1

	if(serverState[connection] != GETRESP_SERVER_STATE)
	{
		if( (connection+1) < MAX_SRV_CLIENTS)
			connection++;
		else
			connection = 0;
	}

	switch(serverState[connection])
	{
		case REC_SERVER_STATE:
			//check clients for data
			if (TCPserverClients[connection]) {
				if (TCPserverClients[connection].connected()) {
					if (TCPserverClients[connection].available()) {
						//get data from the client and push it to the UART
						if ((len = TCPserverClients[connection].available()) > 0) {
							byte *ptr = (byte*)malloc(len);
//							TCPserverClients[connection].read(ptr, len);
							rbuf_len[connection] = len;
							TCPserverClients[connection].read(rbuf[connection], len);



//								DPRINTF("Received %d Serverstate[%d] %s \n",len,connection,ServerStateStr[serverState[connection]]);
//								dumpbuff((char *)ptr, len);
//								DPRINTF("R datalen = %d\n", len);
//								Serial.write(TCPserverClients[connection].read());
							Serial_mutex = 1;
							//Flush all serial input
//							DPRINT("Flush serial");
							while(Serial.available()) {
								char ch;
								Serial.readBytes(&ch, 1);
//								DPRINT(".");

							}
//							len =  Serial.write(ptr,len);
							len =  Serial.write(rbuf[connection],len);
							Serial_wr_cntr[connection]++;
							free(ptr);
							sbufIndex[connection] = 0;
							serverState[connection] = GETRESP_SERVER_STATE;
//								DPRINTF("Connection %d Go to  Serverstate %s (send datalen = %d)\n",connection,ServerStateStr[serverState[connection]], len);
							lastSerReceiveTime = millis();
							retryCntr[connection] = 0;
							timeout = 200L;

//								DPRINTF("Now time %dms\n",lastSerReceiveTime);
						}
					}
				}
				else
				{
					TCPserverClients[connection].stop();
					Serial_opened_cntr[connection]--;
					serverState[connection] = IDLE_SERVER_STATE;
				}
			}
			else
			{
				Serial_lostSock_cntr[connection]++;
				Serial_opened_cntr[connection]--;
				serverState[connection] = IDLE_SERVER_STATE;

			}
			break;
		case GETRESP_SERVER_STATE:

			//check UART for data
			if (Serial.available()) {
				len = Serial.available();
				if(len < (sizeof(sbuf[connection])- sbufIndex[connection]))
				{
					Serial.readBytes((char*)&sbuf[connection][sbufIndex[connection]], len);
					sbufIndex[connection] += len;
				}
				lastSerReceiveTime = millis();
				retryCntr[connection] = 1;
				timeout = TIMEOUT_30mS;
				DPRINT("-");
				DPRINT(lastSerReceiveTime);

			}
			else if (millis() - lastSerReceiveTime > timeout /*TIMEOUT_40mS*/)
			{
				Serial_mutex = 0;
				if(sbufIndex[connection] > 0)
				{
					Serial_rd_cntr[connection]++;
					serverState[connection] = SEND_SERVER_STATE;
				}
				else
				{
//					if(!retryCntr[connection])
//					{
//						Serial.write(rbuf[connection],len);
//						lastSerReceiveTime = millis();
//						retryCntr[connection] = 1;
//					}
//					else
					{
						Serial_timeout_cntr[connection]++;
						serverState[connection] = REC_SERVER_STATE;
					}
				}
//					DPRINTF("rec timeout :Connection %d Serverstate %s (received data sbufIndex[connection]=%d (len %d))\n",connection,ServerStateStr[serverState[connection]],sbufIndex[connection],len);
//					dumpbuff((char *)sbuf[connection], sbufIndex[connection]);

			}
			break;
		case SEND_SERVER_STATE:
			if (TCPserverClients[connection]) {
				if (TCPserverClients[connection].connected()) {
					TCPserverClients[connection].write((char*)sbuf[connection], sbufIndex[connection]);
//					delay(1);
					serverState[connection] = REC_SERVER_STATE;
				}
				else
				{
					TCPserverClients[connection].stop();
					Serial_opened_cntr[connection]--;
					serverState[connection] = IDLE_SERVER_STATE;
				}
			}
			else
			{
//				TCPserverClients[connection].stop();
				Serial_opened_cntr[connection]--;
				Serial_lostSock_cntr[connection]++;
				serverState[connection] = IDLE_SERVER_STATE;
			}
			sbufIndex[connection] = 0;
			DPRINTF("Connection %d Serverstate %s\n",connection,ServerStateStr[serverState[connection]]);
			break;
		case IDLE_SERVER_STATE:
			break;

		default:
			serverState[connection] = REC_SERVER_STATE;
			break;

	}
#endif
#endif // PROTOCOL_CONVERTER
}
