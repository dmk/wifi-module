/*
 * LbotSensorReading.h
 *
 *  Created on: 4/23/2018
 *      Author: akunin
 */

#ifndef LBOTSENSORREARING_H_
#define LBOTSENSORREARING_H_

#include "types.h"
#include <ESP8266WiFi.h>

typedef struct {
	boolean tempSet;
	boolean tempHumSet;
	boolean humSet;
	boolean tempPressSet;
	boolean pressSet;
	double temp;
	double tempHum;
	double hum;
	double tempPress;
	double press;
	byte relayState = 0;
} sensor_reading_t;


class LbotSensorReading {
public:
	LbotSensorReading();
	void set_default(void);

	void setTemperature(double);
	void setTemperatureH(double);
	void setHumidity(double);
	void setTemperatureP(double);
	void setPressure(double);
	void setRelayState(byte);

	const String getTemperature() const {
		return m_sensor_reading.tempSet ? String(m_sensor_reading.temp) : "-";
	}

	const String getTemperatureH() const {
		return m_sensor_reading.tempHumSet ?  String(m_sensor_reading.tempHum) : "-";
	}

	const String getHumidity() const {
		return m_sensor_reading.humSet ?  String(m_sensor_reading.hum) : "-";
	}

	const String getTemperatureP() const {
		return m_sensor_reading.tempPressSet ?  String(m_sensor_reading.tempPress) : "-";
	}

	const String getPressure() const {
		return m_sensor_reading.pressSet ?  String(m_sensor_reading.press) : "-";
	}

	const byte getRelayState() const {
		return m_sensor_reading.relayState;
	}


private:
	sensor_reading_t m_sensor_reading;
};


#endif /* LBOTSENSORREARING_H_ */
