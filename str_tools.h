/*
 * str_tools.h
 *
 *  Created on: Apr 23, 2018
 *      Author: admin
 */

#ifndef STR_TOOLS_H_
#define STR_TOOLS_H_

#include "FS.h"

void dumpbuff(char *buff, int N);
void dumpbuff(char *buff, int N, File &f);
void ipToUchar(int ipInt, char *str);

#endif /* STR_TOOLS_H_ */
