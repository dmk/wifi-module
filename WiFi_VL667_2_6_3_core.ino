#include "Arduino.h"
#include <stdio.h>
#include "board_conf.h"
#include "fw_ver.h"
#include "font.h"
#include "types.h"
#include "IoT_HostInteface.h"
#include "LbotConfig_WebInterface.h"
#include "TCP_Server.h"
#include "LbotConfig.h"
#include "LbotSensorReading.h"
#include "spidev.h"
#include "str_tools.h"
#include "LbotGpio.h"
#include "crc16.h"
#include "debugMacros.h"
#include "hostVL667.h"
#include "FS.h"

#if (OLED != 0)
#else
#define my_draw_text(...)
#define my_lcd_clear()
#define my_show_icon(...)
#endif //  OLED




#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>

const int tX = 0;
const int pX = 12;
const int hX = 84;
const int hIconX = hX+34;
const int wifiIcoX = 118;
const int icoW = 10;
const int icoH = 12;
const int ipX = 40;
const int wifiX = 105;
const int relayH = 16;
const int relayW = 16;
const int relayX = 54;


/* Network Settings */
int activatedAp; //is AP active
int activatedSta; //is STA active
long disconnTime = -1; // last time seen disconnected (if still disconnected)
long connTime = 0; // last time connection initiated
long connAttempts = 0; // number of failed connection attempts
#define POLLINTERVAL_GPIO	20 //20ms
#define DURATION_RELAY	10000 //The frequency of relay test
const int durationWifi = 15000; //The frequency of connection test
const int activateApTo = 60000; //Activate AP if not connected to wifi within 1 min
const int connectInterval = 5000; // Try to reconnect after connection interval (if less than durationWifi, thus should not affect process)
boolean ssidFound = false;

LbotConfig lbotconfig;
LbotSensorReading reading;
LbotGpio gpio;
IPAddress myIP;

char WiFi_rssi;



void setup() {
	// start from displey

	// Setup console
#ifdef	PROTOCOL_CONVERTER
	Serial.begin(115200);
#else
	Serial.begin(115200);
#endif

	Serial.setDebugOutput(false);
	Serial1.setDebugOutput(false);
	delay(10);

	DPRINTF("\n%s (model: %s)\n", DEVICE_NAME, DEVICE_MODEL);
	DPRINTF("Version %s (build %s %s)\n", DEVICE_VERSION, __DATE__, __TIME__);

	gpio.begin();
	lbotconfig.begin();


#ifdef PROTOCOL_CONVERTER


#ifndef LOG_ENA
	SPIFFS.remove("/test.txt");
#endif

	while(0 != waitForHostToStart());

//	Serial.println("Host started");

//	while(0 != updateHost(1))
//	{
//		delay(100);
//	}

#endif

	String ssid = lbotconfig.getSsid();
	String pass = lbotconfig.getPassw();
	String hostname = lbotconfig.getHostName();
	int wifihs = lbotconfig.isSoftAP();

	IPAddress ipInt = lbotconfig.getIp();
	String ip = ipInt.toString();

	IPAddress gateInt = lbotconfig.getGate();
	String gateway = gateInt.toString();

	IPAddress maskInt = lbotconfig.getMask();
	String mask = maskInt.toString();

	bool staticIP = lbotconfig.isStaticIpEnable();



//	Serial.printf("Read config: %s /%s /%d /%s /%s /%s /%s /%d\n", ssid.c_str(), pass.c_str(), ((int) wifihs), hostname.c_str(), ip.c_str(), gateway.c_str(), mask.c_str(),(staticIP)?1:0);
	DPRINTF("Read config: %s /%s /%d /%s /%s /%s /%s \n", ssid.c_str(), pass.c_str(), ((int) wifihs), hostname.c_str(), ip.c_str(), gateway.c_str(), mask.c_str());

	WiFi.setAutoConnect(false);
	WiFi.setAutoReconnect(false);
	WiFi.hostname(lbotconfig.getHostName()); // host name shoud be set before begin in order to be applied.
	WiFi.mode(WIFI_STA);
	MDNS.begin(lbotconfig.getHostName());

	//	relay.begin();
	//	relay.off();

	my_lcd_clear();

	my_show_icon(temperature_img, icoW, icoH, 0, LCD_IMGLINE1, LCD_PIXEL_ON);
	my_draw_text(10, LCD_TXTLINE1, "-88.88", LCD_PIXEL_ON, 0);

	my_draw_text(ipX, LCD_TXTLINE1, "000.000.000.000", LCD_PIXEL_ON, 0);
	my_show_icon(wifiap_img, icoW, icoH, wifiIcoX, LCD_IMGLINE1, LCD_PIXEL_ON);

	my_show_icon(temperature_img, icoW, icoH, hIconX, LCD_IMGLINE2, LCD_PIXEL_ON);
	my_show_icon(humidity_img, icoW, icoH, hIconX, LCD_IMGLINE3, LCD_PIXEL_ON);
	my_draw_text(hX, LCD_TXTLINE2, "-88.88", LCD_PIXEL_ON, 0);
	my_draw_text(hX, LCD_TXTLINE3, "888.88", LCD_PIXEL_ON, 0);

	my_show_icon(temperature_img, icoW, icoH, 0, LCD_IMGLINE2, LCD_PIXEL_ON);
	my_show_icon(pressure_img, icoW, icoH, 0, LCD_IMGLINE3, LCD_PIXEL_ON);
	my_draw_text(12, LCD_TXTLINE2, "-88.88", LCD_PIXEL_ON, 0);
	my_draw_text(12, LCD_TXTLINE3, "8888.88", LCD_PIXEL_ON, 0);

	my_show_icon(relayoff_img, relayW, relayH, relayX, LCD_IMGLINE2, LCD_PIXEL_ON);

	delay(500);
	my_lcd_clear();

	if (!SPIFFS.begin()) {
		DPRINTLN("Failed to mount file system");
//		return (0);
	}


	FSInfo fs_info;
	SPIFFS.info(fs_info);

	if ((fs_info.totalBytes - fs_info.usedBytes) < 1000) {
		DPRINTLN("Memory Full");
//	return (0);
	}else
	{
		DPRINT("Free space ");
		DPRINTLN(fs_info.totalBytes - fs_info.usedBytes);
	}


	//Init the Spiff FS (check if our INIT file exists or not
	File f = SPIFFS.open("init1", "r+");

	if (!f) {
		DPRINTLN("Formatting Spiff...");
		delay(200);
		//Format
		SPIFFS.format();

		//Make a Init File
		File f1 = SPIFFS.open("init1", "w+");
		f1.println("init1");
		f1.close();

		DPRINTLN("Done");
		delay(200);
	}
	if(SPIFFS.exists("/temp.log"))
	{
		SPIFFS.remove("/temp.log");
		DPRINTLN("Remove old log file");
	}


	//RELAY
	my_show_icon(relayIconOn ? relayon_img : relayoff_img, relayW, relayH, relayX, LCD_IMGLINE2, LCD_PIXEL_ON);
}
/////////////  Main Loop  ///////////////
void loop() {
	long now = millis();

	if(checkWifiConnection(now)) {
		pubSubConnection(now);
		tcpConnection(now);
	}

	gpioLoop(now);

}
/////////////  Main Loop  End ///////////////

void gpioLoop(long now) {
	static long lastGpioTime = 0; //The last gpio check
	byte portVal;

	if( now - lastGpioTime > POLLINTERVAL_GPIO ){
		lastGpioTime = now;

		byte bitsChanged = gpio.pollDI(portVal);
		if(bitsChanged & (1<<INPUT_PORT0)) {
			sendDataToHost(DIGINPUT0_TYPE, (portVal>>INPUT_PORT0)&0x01);
		}
		if(bitsChanged & (1<<INPUT_PORT1)) {
			sendDataToHost(DIGINPUT1_TYPE, (portVal>>INPUT_PORT1)&0x01);
		}
	}
}




boolean checkWifiConnection(long now){
	static long lastWifi = 0; //The last wifi check

	if( now - lastWifi > durationWifi ){
		lastWifi = now;  //Remember the last time measurement
		activatedSta = (WiFi.status() == WL_CONNECTED)?true:false;

		if(activatedSta) {
			if(lbotconfig.isSoftAP() && !activatedAp) {
				DPRINTLN("Activating AP");
				activatedAp	= WiFi.softAP(lbotconfig.getHostName());
				delay(100);
				if(activatedAp) {
					DPRINTLN("AP activated");
				}
			} else if(!lbotconfig.isSoftAP() && activatedAp) {
				DPRINTLN("Deactivating AP");
				activatedAp	= !WiFi.softAPdisconnect(true);
				delay(100);
				if(!activatedAp) {
					DPRINTLN("AP deactivated");
				}
			}
			myIP = WiFi.localIP();
			String ipAddr = myIP.toString();

			int length = ipAddr.length();
			int ipCoordX = ((length-3)*5)+11;
			char* ip = (char*) ipAddr.c_str();
			my_draw_text(wifiIcoX - ipCoordX, LCD_TXTLINE1, ip, LCD_PIXEL_ON, 0);
			my_show_icon(activatedAp ? wifiap_img : wifi_img, icoW, icoH, wifiIcoX, LCD_IMGLINE1, LCD_PIXEL_ON);


			long rssi = WiFi.RSSI();
			WiFi_rssi = (char)rssi;
			String currSSID = WiFi.SSID();
			DPRINTF("Activated Status %s, ipAddr = %s(SSID %s - rssi %ddB)\n",activatedSta?"TRUE" :"FALSE",ipAddr.c_str(),currSSID.c_str(),rssi);
			if(!IsWebStarted())
				setupWeb();
			disconnTime = 0;
			connAttempts = 0;
			connTime = 0;
			if(rssi>=31)
			{
				activatedSta = false;
			}
		}
		if(!activatedSta)
		{
			DPRINTF("Activated Status %s\n",activatedSta?"TRUE" :"FALSE");
			if(!activatedAp) {
				if(lbotconfig.isSoftAP() || (disconnTime > 0 && now - disconnTime > activateApTo)) { //if dTo == 0, connection have just been lost
//					DPRINT("Access Point: ");
//					// WiFi.softAPConfig(IPAddress(0,0,0,0), IPAddress(0,0,0,0), IPAddress(0,0,0,0));
//					activatedAp	= WiFi.softAP(lbotconfig.getHostName());
//					IPAddress myIP_AP = WiFi.softAPIP();
//
//					DPRINTLN(activatedAp ? "Activated" : "Failed");
//					DPRINT("AP Router IP: ");
//					DPRINTLN(myIP_AP);
//
//					String ipAddr = myIP_AP.toString();
//					int length = ipAddr.length();
//					int ipCoordX = ((length-3)*5)+11;
//					char* ip = (char*) ipAddr.c_str();
//					my_draw_text(wifiIcoX - ipCoordX, LCD_TXTLINE1, ip, LCD_PIXEL_ON, 0);
//					my_show_icon(wifiap_img, icoW, icoH, wifiIcoX, LCD_IMGLINE1, LCD_PIXEL_ON);
//					delay(100);
//					//					WiFi.softAPdisconnect(false);
				} else {
					my_draw_text(ipX, LCD_TXTLINE1, "               ", LCD_PIXEL_ON, 0);
					my_show_icon(clear_img, icoW, icoH, wifiIcoX, LCD_IMGLINE1, LCD_PIXEL_ON);
					myIP.fromString("0.0.0.0");
				}
			}
			// resetting if necessary
			setupWeb();
			if(disconnTime <= 0) { //start counting disconnect time out (dTo
				disconnTime = now;
			}
			int scanCompleted = WiFi.scanComplete();
			if(scanCompleted >= 0) {
				ssidFound = false;
				for(int i = 0; i < scanCompleted; i++) {
					String ssid = WiFi.SSID(i);
					DPRINT("SSID: " + ssid);
					DPRINTF("   RSSI: %d dBm\n", WiFi.RSSI(i));

					if(String(lbotconfig.getSsid()) == ssid) {
						ssidFound = true;
						DPRINTLN("Discovered SSID: " + ssid);
						break;
					}
				}
				WiFi.scanDelete();
			} else if(scanCompleted == -2) {
				if(strcmp(lbotconfig.getSsid(), "") != 0) {
					WiFi.scanNetworks(true, false);
					DPRINTLN("Initiating scanning");
				} else {
					connAttempts = 0;
				}
				ssidFound = false;
			} else if(scanCompleted == -1) {
				DPRINTLN("Scanning...");
			}

			if(ssidFound && now - connTime > connectInterval) {

				DPRINTF("Connection to SSID: %s\n",lbotconfig.getSsid());
				DPRINTF("Activated Status %s\n",activatedSta?"TRUE" :"FALSE");

				if(connTime <= 0) {
					connTime = now;
				}
				if(connAttempts++ < 3) {

					DPRINTF("Attempt: %d\n", connAttempts);
					if(lbotconfig.isStaticIpEnable()) {
						DPRINTF("Static IP: \n",lbotconfig.getIp());
						WiFi.config(lbotconfig.getIp(), lbotconfig.getGate(), lbotconfig.getMask(), IPAddress(8,8,8,8));
					}
					WiFi.begin(lbotconfig.getSsid(), lbotconfig.getPassw());
//					WiFi.begin("MMTEST", "12345678");
				} else {
					String ssid = "";
					lbotconfig.setSsid((char*)ssid.c_str(),ssid.length());
				}
			}
		}
#ifdef PROTOCOL_CONVERTER
		updateHost(0);
#endif
	}
	if(activatedAp || activatedSta) {
		loopWeb();

	}
	return activatedSta;
}

void tcpConnection(long now){
	static int tcp_server_run = 0;

	if(activatedSta) { // we have to run web with no wifi connectin in case we have hot spot running
		if(tcp_server_run == 0) {
			startTcpServer();
			DPRINTF("Start TCP server");
			tcp_server_run = 1;
		}
		if(tcp_server_run) {
			loop_tcp_server();
		}
	}
}

void pubSubConnection(long now){
#if (HOSTING  == PUBSUB)
	handlePubSub(now);
#endif
}

void setupOled () {
}



