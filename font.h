/**************************************************************************
 *
 * FILE NAME:			font.h
 * FILE DESCRIPTION:	Font types definitions
 *
 * FILE CREATION DATE:	24-07-2009
 *
 *==========================================================================
 *
 * Modification history:
 * --------------------
 * 01a,24jul09 erd written
 *
 ***************************************************************************/

#ifndef __FONT_H_
#define __FONT_H_

#include "types.h"

// ==========================================================================
// structure definition
// ==========================================================================

// This structure describes a single character's display information
typedef struct
{
	const unsigned char widthBits;					// width, in bits (or pixels), of the character
	const unsigned char highBits;					// width, in bits (or pixels), of the character
	const unsigned int offset;		// offset of the character's bitmap, in bytes, into the the FONT_INFO's data array

} FONT_CHAR_INFO;

// Describes a single font
typedef struct
{
	const unsigned char heightPages;	// height, in pages (8 pixels), of the font's characters
	const unsigned char startChar;		// the first character in the font (e.g. in charInfo and data)
	const unsigned char endChar;		//
	const FONT_CHAR_INFO* charInfo;		// pointer to array of char information
	const unsigned char* data;			// pointer to generated array of character visual representation

} FONT_INFO;

#include "ArialNarrow10_bold.h"
//#include "ArialNarrow8.h"
#include "ArialNarrow9.h"
#include "CourierNew10_bold.h"
#include "Miriam10_bold.h"
#include "SwitzerlandNarrow10_bold.h"
#include "glcd-bitmaps.h"

#endif /* __FONT_H_ */

