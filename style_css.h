/*
 * style_css.h
 *
 *  Created on: Feb 7, 2020
 *      Author: admin
 */

#ifndef STYLE_CSS_H_
#define STYLE_CSS_H_


const char* style = R"rawliteral(

body {
  margin: 0;
}

.lbot-title-1 {
    text-align: center;
    padding-top: 15px;
}
.lbot-title-2 {
padding: 15px 0 0 20px;
}

.grid-container {
    display: grid;
    grid-template-columns: auto auto;
}
    .grid-container > div {
/*        background-color: rgba(255, 255, 255, 0.8);*/
        text-align: left;
        padding: 20px 0;
/*        font-size: 10px;*/
    }

.item1 {
    grid-row: 1 / span 10;
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 10%;
    background-color: #f1f1f1;
    position: fixed;
    height: 100%;
    overflow: auto;
}

li a {
  display: block;
  color: #000;
  padding: 8px 16px;
  text-decoration: none;
}

li a.active {
  background-color: #4CAF50;
  color: white;
}

li a:hover:not(.active) {
  background-color: #555;
  color: white;
}

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 10px 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 14px;
    margin: 4px 2px;
    cursor: pointer;
}

.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #4CAF50;
  color: white;
  text-align: center;
}

table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
/*
input[type=checkbox] {
	visibility: hidden;
}
.coolCheckbox {
    width: 116px;
    height: 35px;
    background: #3f51b5;
    border-radius: 30px;
    position: relative;
}
.coolCheckbox:before {
    content: 'On';
    position: absolute;
    top: 9px;
    left: 13px;
    height: 2px;
    color: #fff;
    font-size: 16px;
}
.coolCheckbox:after {
    content: 'Off';
    position: absolute;
    top: 9px;
    left: 84px;
    height: 2px;
    color: #fff;
    font-size: 16px;
}
.coolCheckbox label {
    display: block;
    width: 52px;
    height: 25px;
    border-radius: 50px;
    transition: all .5s ease;
    cursor: pointer;
    position: absolute;
    top: 5px;
    z-index: 1;
    left: 5px;
    background: #ddd;
}
.coolCheckbox span {
    font-size: 16px;
    display: block;
    width: 160px;
    height: 28px;
    position: absolute;
    top: 9px;
    right: -166px;
}
.coolCheckbox input[type=checkbox]:checked + label {
    left: 60px;
    background: #41ff51;
}
*/
)rawliteral";

#endif /* STYLE_CSS_H_ */
