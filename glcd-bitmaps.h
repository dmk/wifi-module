/***************************
* Bitmap and Fonts
****************************/
#ifndef GLCD_BITMAPS_H
#define GLCD_BITMAPS_H

extern const unsigned char clock_bigBitmap[];
//extern const unsigned char clockBitmap[];
extern const unsigned char up_img[];
extern const unsigned char down_img[];
extern const unsigned char wifi_img[];
extern const unsigned char wifiap_img[];
extern const unsigned char clear_img[];
extern const unsigned char temperature_img[];
extern const unsigned char humidity_img[];
extern const unsigned char pressure_img[];
extern const unsigned char relayon_img[];
extern const unsigned char relayoff_img[];
//extern const unsigned char batt_img[];
//extern const unsigned char battmeter_img[];
//extern const unsigned char bell_img[];
//extern const unsigned char pause_img[];
//extern const unsigned char run_img[];
//extern const unsigned char ac_img[];
extern const unsigned char pmtr_leftBitmap[];
extern const unsigned char pmtr_rightBitmap[];
extern const unsigned char pmtr_upBitmap[];
extern const unsigned char pmtr_downBitmap[];
extern const unsigned char pmtr_0Bitmap[];
extern const unsigned char pmtr_01Bitmap[];
extern const unsigned char pmtr_02Bitmap[];
extern const unsigned char pmtr_03Bitmap[];
extern const unsigned char pmtr_04Bitmap[];
extern const unsigned char pmtr_05Bitmap[];
extern const unsigned char pmtr_06Bitmap[];
extern const unsigned char pmtr_07Bitmap[];
extern const unsigned char pmtr_08Bitmap[];
extern const unsigned char pmtr_09Bitmap[];
extern const unsigned char pmtr_10Bitmap[];
extern const unsigned char pmtr_11Bitmap[];
extern const unsigned char pmtr_12Bitmap[];
extern const unsigned char pmtr_13Bitmap[];
extern const unsigned char pmtr_14Bitmap[];
extern const unsigned char pmtr_15Bitmap[];
extern const unsigned char pmtr_16Bitmap[];

extern const FONT_INFO arial9ptFontInfo;
extern const FONT_INFO arialNarrow11ptFontInfo;
extern const FONT_INFO arialNarrow8ptFontInfo;
//extern const unsigned char arial9ptCharBitmaps[];

//extern const unsigned char arial11ptCharBitmaps[];


#endif
