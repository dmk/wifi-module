/*
 * LbotEeprom.h
 *
 *  Created on: Mar 8, 2018
 *      Author: dkunin
 */

#ifndef LBOTEEPROM_H_
#define LBOTEEPROM_H_
#include "Arduino.h"
#include "types.h"

#define EEPROM_SIZE 512

class LbotEeprom {
 public:
	LbotEeprom();
	void begin(void);
	uchar read(char *ptr,int idx, int len);
	uchar write(char *ptr,int idx, int len);
 private:
//  boolean readData(void);
//  float humidity, temp;
};

#endif /* LBOTEEPROM_H_ */
