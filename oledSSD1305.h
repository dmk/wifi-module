#ifndef oled_h
#define oled_h

#include "board_conf.h"

#ifdef OLED_PUB
#define OLEDPUB
#else
#define OLEDPUB extern
#endif

#define OLED_DC_BIT	5
#define OLED_RESET_BIT 6
#define OLED_POWER_BIT 7

#define OLED_DATA 1
#define OLED_CMD 0

#define OLED_RESET	   	OLED_RESET_PIN = 0; OLED_RESET_PIN = 1
#define OLED_DC_DAT    	OLED_DATACMD_PIN = 1
#define OLED_DC_CMD    	OLED_DATACMD_PIN = 0
#define OLED_CS_ON     	OLED_CS_PIN = 0
#define OLED_CS_OFF    	OLED_CS_PIN = 1
#define OLED_POWER_ON  	OLED_POWER_PIN = 1
#define OLED_POWER_OFF 	OLED_POWER_PIN = 0

#ifdef SSD1305

#define	DISPLOFF_OLEDCMD			0xAE	/* Set display Off */
#define	SETCLCKF_OLEDCMD			0xD5 	/* Set display clock divide ratio/oscillatir frequency */
#define	SETMUXRAT_OLEDCMD			0xA8 	/* Set display multiplex ratio */
#define	SETDISPOFFSET_OLEDCMD	  	0xD3	/* Set display Offset */
#define	SETSTARTLIN_OLEDCMD(line)	(0x40 | (0x3F & line))	/* Set display Start Line */
#define	SETSEGRMP_OLEDCMD(map)		(0xA0 | (0x01 & map))	/* Set segment re-map */
#define	SETCOMDIR_OLEDCMD(dir)		(0xC0 | (0x08 & dir))	/* Set COM Output Scan Direction */
#define	MASTERCFG_OLEDCMD			0xAD 	/* Set Master configuration */

#elif defined(SSD1325)

#define	DISPLOFF_OLEDCMD			0xAE	/* Set display Off */
#define	SETCLCKF_OLEDCMD			0xD5 	/* Set display clock divide ratio/oscillatir frequency */
#define	SETMUXRAT_OLEDCMD			0xA8 	/* Set display multiplex ratio */
#define	SETDISPOFFSET_OLEDCMD	  	0xD3	/* Set display Offset */
#define	SETSTARTLIN_OLEDCMD(line)	(0x40 | (0x3F & line))	/* Set display Start Line */
#define	SETSEGRMP_OLEDCMD(map)		(0xA0 | (0x01 & map))	/* Set segment re-map */
#define	SETCOMDIR_OLEDCMD(dir)		(0xC0 | (0x08 & dir))	/* Set COM Output Scan Direction */
#define	MASTERCFG_OLEDCMD			0xAD 	/* Set Master configuration */
#endif

#if 0
#define	0xD8, 0x05,	 	/* Set Area Color Mode on/off & Low Power Display Mode  */
#define	0x91, 0x3F,	 	/* Set Look up Table */
#define	0x3F, 0x3F, 0x3F,
#define	0x81, 0x8F,	 	/* Set Current Control for Bank 0 */
#define	0xD9, 0x11,	 	/* Set Pre-charge Period */
#define	0xDA, 0x12,	 	/* Set COM Pins Hardware configuration */
#define	0xDB, 0x34,	 	/* Set VCOMH Detect Level */
#define	0xA4,				 	/* Set Entire Display On/Off */
#define	0xA6,				 	/* Set Normal Inverse Display */
/*	Clear_screen ???? */
#define	0xAF,			 		/* Set display On */
#endif

void init_oleddispl(void);
void close_oleddispl(void);
void writecmd_oleddispl(unsigned char *cmd, int len);
OLEDPUB void writedata_oleddispl(unsigned char data);
OLEDPUB void oleddispl_on(unsigned char mode);
#endif	/* oled_h */

