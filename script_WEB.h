/*
 * script_WEB.h
 *
 *  Created on: Feb 5, 2020
 *      Author: admin
 */

#ifndef SCRIPT_WEB_H_
#define SCRIPT_WEB_H_

const char* script_old = // "$(function(){$('#savePass').click(function(){''===$('#ssid').val()?alert('Password Required'):($('.mdl-button').prop('disabled',!0),$.ajax({url:'/savePass',type:'POST',data:$('#savePassForm').serialize(),contentType:'application/x-www-form-urlencoded; charset=UTF-8'}).done(function(){setTimeout(function(){$('.mdl-button').prop('disabled',!1)},1e3)}).fail(function(){alert('Error')}))}),$('#save').click(function(){$('.mdl-button').prop('disabled',!0),$.ajax({url:'/save',type:'POST',data:$('#saveForm').serialize(),contentType:'application/x-www-form-urlencoded; charset=UTF-8'}).done(function(){setTimeout(function(){$('.mdl-button').prop('disabled',!1)},1e3)}).fail(function(){alert('Error')})}),$('#reboot').click(function(){$('.mdl-button').prop('disabled',!0),$.ajax({url:'/reboot',type:'POST'}).done(function(){setTimeout(function(){$('#relayStateLabel').removeClass('is-checked'),$('#relayState').prop('checked',!1),$('.mdl-button').prop('disabled',!1)},1e3)}).fail(function(){alert('Error')})}),$('#refreshReadings').click(function(){$('.mdl-button').prop('disabled',!0),$.ajax({url:'/refreshReadings',type:'POST'}).done(function(a,b,c){$('#temper').text(c.getResponseHeader('Temper')),$('#temperH').text(c.getResponseHeader('TemperH')),$('#humid').text(c.getResponseHeader('Humid')),setTimeout(function(){$('.mdl-button').prop('disabled',!1)},1e3)}).fail(function(a,b){$('.mdl-button').prop('disabled',!1),alert('Error: '+b)})}),$('#runUpdate').click(function(){$('.mdl-button').prop('disabled',!0);var b=$('#uploadBtn')[0].files[0],c=new FormData;c.append('img',b),$.ajax({url:'/runUpdate',type:'POST',data:c,enctype:'multipart/form-data',processData:!1,contentType:!1,cache:!1,timeout:3e5}).done(function(){setTimeout(function(){$('.mdl-button').prop('disabled',!1)},1e3)}).fail(function(d,e){$('.mdl-button').prop('disabled',!1),alert('Error: '+e)})}),$('#uploadBtn').change(function(){null!=this.files[0]&&$('#uploadFile').val(this.files[0].name)})});";

"$(function () {\n"\
"    $('#savePass').click(function () {\n"\
"        if($('#ssid').val() !== '') {\n"\
"            $('.mdl-button').prop('disabled', true);\n"\
"            $.ajax({\n"\
"                 url: '/savePass',\n"\
"                 type: 'POST',\n"\
"                 data: $('#savePassForm').serialize(),\n"\
"                 contentType: 'application/x-www-form-urlencoded; charset=UTF-8'\n"\
"            }).done(function( msg ) {\n"\
"                 setTimeout(function(){\n"\
"                     $('.mdl-button').prop('disabled', false);\n"\
"                            }, 1000);\n"\
"            }).fail(function() {\n"\
"                     alert('Error');\n"\
"            });\n"\
"        } else {\n"\
"            alert('Password Required');\n"\
"        }\n"\
"    });\n"\

"        $('#save').click(function () {\n"\
"            $('.mdl-button').prop('disabled', true);\n"\
"            $.ajax({\n"\
"                url: '/save',\n"\
"                type: 'POST',\n"\
"                data: $('#saveForm').serialize(),\n"\
"                contentType: 'application/x-www-form-urlencoded; charset=UTF-8'\n"\
"            }).done(function( msg ) {\n"\
"                $('.mdl-button').prop('disabled', false);\n"\
"            }).fail(function() {\n"\
"                alert('Error');\n"\
"                $('.mdl-button').prop('disabled', false);\n"\
"            });\n"\
"        });\n"\

"        $('#mqttSave').click(function () {\n"\
"            $('.mdl-button').prop('disabled', true);\n"\
"            $.ajax({\n"\
"                url: '/mqttSave',\n"\
"                type: 'POST',\n"\
"                data: $('#mqttSaveForm').serialize(),\n"\
"                contentType: 'application/x-www-form-urlencoded; charset=UTF-8'\n"\
"            }).done(function( msg ) {\n"\
"                $('.mdl-button').prop('disabled', false);\n"\
"            }).fail(function() {\n"\
"                alert('Error');\n"\
"                $('.mdl-button').prop('disabled', false);\n"\
"            });\n"\
"        });\n"\

"        $('#reboot').click(function () {\n"\
"            $('.mdl-button').prop('disabled', true);\n"\
"            $.ajax({\n"\
"                url: '/reboot',\n"\
"                type: 'POST'\n"\
"            }).done(function( msg ) {\n"\
"                setTimeout(function(){\n"\
"                    $('#relayStateLabel').removeClass('is-checked');\n"\
"                    $('#relayState').prop('checked', false);\n"\
"                    $('.mdl-button').prop('disabled', false);\n"\
"                }, 1000);\n"\
"            }).fail(function() {\n"\
"                alert('Error');\n"\
"            });\n"\
"         });\n"\

"         $('#refreshReadings').click(function () {\n"\
"             $('.mdl-button').prop('disabled', true);\n"\
"             $.ajax({\n"\
"                 url: '/refreshReadings',\n"\
"                 type: 'POST',\n"\
"             }).done(function(data, textStatus, jqXHR) {\n"\
"                 $('#temper').text(jqXHR.getResponseHeader('Temper'));\n"\
"                 $('#temperH').text(jqXHR.getResponseHeader('TemperH'));\n"\
"                 $('#humid').text(jqXHR.getResponseHeader('Humid'));\n"\
"                 setTimeout(function(){\n"\
"                     $('.mdl-button').prop('disabled', false);}, 1000);\n"\
"             }).fail(function(jqXHR, textStatus) {\n"\
"                 $('.mdl-button').prop('disabled', false);\n"\
"                 alert('Error: ' + textStatus);\n"\
"             });\n"\
"          });\n"\

"          $('#runUpdate').click(function (event) {\n"\
"              $('.mdl-button').prop('disabled', true);\n"\
"              var file = $('#uploadBtn')[0].files[0];\n"\
"              var formdata = new FormData();\n"\
"              formdata.append('img', file);\n"\
"              $.ajax({\n"\
"                  url: '/runUpdate',\n"\
"                  type: 'POST',\n"\
"                  data: formdata,\n"\
"                  enctype: 'multipart/form-data',\n"\
"                  processData: false,\n"\
"                  contentType: false,\n"\
"                  cache: false,\n"\
"                  timeout: 300000\n"\
"              }).done(function( msg ) {\n"\
"                  setTimeout(function(){\n"\
"                     $('.mdl-button').prop('disabled', false);\n"\
"                  }, 1000);\n"\
"              }).fail(function(jqXHR, textStatus) {\n"\
"                  $('.mdl-button').prop('disabled', false);\n"\
"                  alert('Error: ' + textStatus);\n"\
"              });\n"\
"           });\n"\

"           $('#uploadBtn').change(function () {\n"\
"              if(this.files[0] != null) {\n"\
"                  $('#uploadFile').val(this.files[0].name);\n"\
"              }\n"\
"           });\n"\
"});\n";



const char* script = R"rawliteral(

function saveSSID() {
    var xhttp = new XMLHttpRequest();
    var ssid = document.getElementById('ssid');
    var passwd = document.getElementById('passw');
    if (ssid.value === '') {
        alert('SSID is missing');
    }else{
        xhttp.open('POST', '/savePass', true);
        xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhttp.send('ssid=' + ssid.value + '&passw=' + passwd.value;

        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                alert('SSID saved');
            }
        };
    }
}

function saveIP() {
    var xhttp = new XMLHttpRequest();
    var ip = document.getElementById('ip');
    var mask = document.getElementById('mask');
    var gateway = document.getElementById('gateway');
    var hostname = document.getElementById('hostname');
    var staticIp = document.getElementById('staticIp');
    var softAp = document.getElementById('softAp');
    xhttp.open('POST', '/save', true);
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhttp.send('ip=' + ip.value + '&mask=' + mask.value + '&gateway=' + gateway.value + '&hostname=' + hostname.value + '&staticIp=' + staticIp.value + '&softAp=' + softAp.value);
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            alert('IP saved');
        }
    };
}
function reboot() {
    var xhttp = new XMLHttpRequest();

    xhttp.open('POST', '/reboot', true);
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhttp.send();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            alert('reboot');
        }
    };
}

)rawliteral";



#endif /* SCRIPT_WEB_H_ */
