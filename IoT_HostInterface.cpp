/*
 * HostInterface.cpp
 *
 *  Created on: Mar 5, 2018
 *      Author: dkunin
 */

#include <stdio.h>
#include "board_conf.h"
#include "LbotConfig.h"
#include "LbotSensorReading.h"
#include "LbotGpio.h"
#include "types.h"
#include "IoT_HostInteface.h"

#include <ESP8266WiFi.h>
#if (HOSTING == THINGSPEAK)
WiFiClient wfclient;
#endif
extern LbotConfig lbotconfig;
extern LbotSensorReading reading;
extern LbotGpio gpio;

#if (HOSTING == THINGSPEAK)
// thingspeak API key,
#define server "api.thingspeak.com"
String apiKey = "RFDRTQSKNBNWKL2L";
#endif

#if (HOSTING == PUBSUB)
#include <PubSubClient.h>

/* PUBSUB Topic Settings */
const char*  temperature_topic = "/temperature";
const char*  humidity_topic = "/humidity";
const char*  humidity_temperature_topic = "/humidity_temperature";
const char*  pressure_topic = "/pressure";
const char*  pressure_temperature_topic = "/pressure_temperature";
const char*  relay_topic = "/relay";
const char*  DI0_topic = "/di0";
const char*  DI1_topic = "/di1";
const char*  DO0_topic = "/do0";
const char*  DO1_topic = "/do1";
long lastAttempt = 0;
const int connIntervalPubSub = 15000; //The frequency of connection test
const int connLongIntervalPubSub = 900000; //The frequency of connection test if too many unsuccessful attempts
int connectionAttempt = 0;
boolean connectionOk = false;

//IPAddress dataServer = IPAddress(192,168,1,32);
//PubSubClient PubSub_Client(dataServer, 1883, wfclient);
//int port = lbotconfig.getDataServerPort();
PubSubClient pubSubClient(wfclient) ;

#endif

#if (HOSTING == THINGSPEAK)
void sendThingSpeakData(char *field, double data) {
	if (wfclient.connect(server,80)) { // "184.106.153.149" or api.thingspeak.com
		//      char temperatureString[7];
		//      dtostrf(tempDev[0], 2, 2, temperatureString);
		String postStr = apiKey;
		postStr += "&";
		postStr += field;
		postStr += "=";
		postStr += String(data);
		postStr += "\r\n\r\n";

		wfclient.print("POST /update HTTP/1.1\n");
		wfclient.print("Host: api.thingspeak.com\n");
		wfclient.print("Connection: close\n");
		wfclient.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
		wfclient.print("Content-Type: application/x-www-form-urlencoded\n");
		wfclient.print("Content-Length: ");
		wfclient.print(postStr.length());
		wfclient.print("\n\n");
		wfclient.print(postStr);
	}
	wfclient.stop();
}
#endif

void sendDataToHost(sensor_data_t type, double value) {
#if (HOSTING == PUBSUB)
	if(pubSubClient.connected()) {
		String topicPath = String(lbotconfig.getTopicPath());
		String textValue = String(value);
		switch(type) {
		case HUM_TYPE:
			topicPath += String(humidity_topic);
			break;
		case TEMP_HUM_TYPE:
			topicPath += String(humidity_temperature_topic);
			break;
		case TEMP_TYPE:
			topicPath += String(temperature_topic);
			break;
		case PRESS_TYPE:
			topicPath += String(pressure_topic);
			break;
		case TEMP_PRESS_TYPE:
			topicPath += String(pressure_temperature_topic);
			break;
		case RELAY_TYPE:
			topicPath += String(relay_topic);
			textValue = String((int)value); // overwrite to send int value, not double
			break;
		case DIGINPUT0_TYPE:
			topicPath += String(DI0_topic);
			textValue = String((int)value); // overwrite to send int value, not double
			break;
		case DIGINPUT1_TYPE:
			topicPath += String(DI1_topic);
			textValue = String((int)value); // overwrite to send int value, not double
			break;
		default:
			break;
		}
		Serial.println(topicPath + ": " + textValue);
		pubSubClient.publish(topicPath.c_str(), textValue.c_str());
	}
#endif
#if (HOSTING == THINGSPEAK)
	switch(type) {
	case HUM_TYPE:
		sendThingSpeakData("field4", value);
		break;
	case TEMP_HUM_TYPE:
		sendThingSpeakData("field3", value);
		break;
	case TEMP_TYPE:
		sendThingSpeakData("field1", value);
		break;
	default:
		break;
	}
#endif
}

#if (HOSTING == PUBSUB)
#define BUFFER_SIZE 100
void callback(char* topic, byte* payload, unsigned int length) {
#if 1
	Serial.print("Message arrived [");
	Serial.print(topic);
	Serial.print("] ");
	for(unsigned int i = 0; i < length; i++) {
		Serial.print((char)payload[i]);
	}
	Serial.println();

	String topicPath = String(lbotconfig.getTopicPath());
	String relayPath = (topicPath + String(relay_topic));
	String do0Path = (topicPath + String(DO0_topic));
	String do1Path = (topicPath + String(DO1_topic));
	if(strcmp(topic, relayPath.c_str()) == 0) {
		int command = String((char)payload[0]).toInt();
		Serial.printf("Relay: %d", command);
		if(command != relay.isOn()) {
			if(command == ON || command == OFF) {
				reading.setRelayState(command);
				if(command) {
					relay.on();
				} else {
					relay.off();
				}
				Serial.print(command ? " - ON" : " - OFF");
				Serial.printf(" New State: %d", relay.isOn());
			} else {
				Serial.print(" - ?");
			}
		}
		Serial.println();
	}else if(strcmp(topic, do0Path.c_str()) == 0) {
		if((char)payload[0] == '0')	{
			gpio.clrDO0();
		} else {
			gpio.setDO0();
		}

	}else if(strcmp(topic, do1Path.c_str()) == 0) {
		if((char)payload[0] == '0')	{
			gpio.clrDO1();
		} else {
			gpio.setDO1();
		}

	}

#else
	if(pub.has_stream())
	{
		uint8_t buf[BUFFER_SIZE];
		int read;
		while(read = pub.payload_stream().read(buf, BUFFER_SIZE))
		{
			Serial.write(buf, read);
		}
		pub.payload_stream().stop();
		Serial.println("");
	}
	else
	{
		Serial.print("Message: ");
		//    pub.topic
		Serial.println(pub.payload_string());

		//		if(pub.payload_string().toInt())
		//		setLed();
		//		else
		//		clrLed();

		String values = pub.payload_string();
		int c1 = pub.payload_string().indexOf(',');
		int c2 = pub.payload_string().indexOf(',', c1 + 1);
		int red = pub.payload_string().toInt();
		int green = pub.payload_string().substring(c1 + 1).toInt();
		int blue = pub.payload_string().substring(c2 + 1).toInt();
	}
	//	updateLights = true;
#endif
}

void handlePubSub(long now) {
	// 5 unsuccessful attempts in 15 seconds interval, then switch to 15 min interval
	if(!connectionOk && (now - lastAttempt > (connectionAttempt < 5 ? connIntervalPubSub : connLongIntervalPubSub))){
		if (!pubSubClient.connected()) {
			char *host = (char*) lbotconfig.getDataServerHost();
			int port = lbotconfig.getDataServerPort();
			char* user = (char*) lbotconfig.getDataServerUsername();
			char* pass = (char*) lbotconfig.getDataServerPass();
			pubSubClient.setServer(host, port);
			Serial.printf("Connection to MQTT - %s:%d\n", host, port);
			boolean connected;
			if(strlen(user) > 0 && strlen(pass) > 0) {
				connected = pubSubClient.connect(lbotconfig.getHostName(), user, pass);
			} else {
				connected = pubSubClient.connect(lbotconfig.getHostName());
			}
			if (connected) {
				String topicPath = String(lbotconfig.getTopicPath());
				pubSubClient.setCallback(callback);
				// Once connected, publish an announcement...
				pubSubClient.publish(lbotconfig.getHostName(), "Hello!");
				// ... and re-subscribe
				String relayPath = topicPath + String(relay_topic);
				Serial.println("Subscribe to Relay Topic: " + relayPath);
				if(lastAttempt == 0) {
					sendDataToHost(RELAY_TYPE, 0); // turn off switch on all controlling devices before subscribing. (only after reboot)
				}
				pubSubClient.subscribe(relayPath.c_str(), 0);

				String do0Path = topicPath + String(DO0_topic);
				Serial.println("Subscribe to IO Topic: " + do0Path);
//				if(lastAttempt == 0) {
//					sendDataToHost(RELAY_TYPE, 0); // turn off switch on all controlling devices before subscribing. (only after reboot)
//				}
				pubSubClient.subscribe(do0Path.c_str(), 0);

				String do1Path = topicPath + String(DO1_topic);
				Serial.println("Subscribe to IO Topic: " + do1Path);
//				if(lastAttempt == 0) {
//					sendDataToHost(RELAY_TYPE, 0); // turn off switch on all controlling devices before subscribing. (only after reboot)
//				}
				pubSubClient.subscribe(do1Path.c_str(), 0);

				connectionAttempt = 0;
				connectionOk = true;
				Serial.println("MQTT - OK");
			} else {
				if(connectionAttempt < 5) {
					connectionAttempt++;
				}
				connectionOk = false;
				Serial.println("FAILED");
			}
		}
		lastAttempt = now;
	}
	if (pubSubClient.connected()) {
		pubSubClient.loop();
	} else {
		connectionOk = false;
	}
}
#endif
