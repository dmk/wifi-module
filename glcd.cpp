/*****************************************************
 *                 glcd.c
 *
 * lcd driver for KS0713 and compatible
 *
 * Parallel Mode Only at this stage!!
 *
 * Target: PIC24 dsPIC30 and dsPIC33 devices
 *         MPLAB-C30 Compiler
 *
 * Author: Michael Pearce <mike@kiwacan.co.nz>
 *
 * Started: 10 Jan 2007
 *
 *
 * Based oncode written by Zelea for Atmel AVR
 * Found at http://nic.ath.cx/graphic-lcd/
 *    
 *
 * This program is provided under the GPL license
 *   http://www.gnu.org/copyleft/gpl.html
 *
 *****************************************************/

/*****************************************************
 * Include glcd.h
 * Modify port settings, processor type etc
 * in glcd.h to suit your specific needs
 *****************************************************/
#include <string.h>
#include "glcd.h"
#include "oledSSD1305.h"

unsigned short position;
const unsigned char *bitmap_ptr_dbg;
unsigned short len_dbg;

static void lcd_send_cmd(u08 cmd, u08 flag)

{

	cmd |= flag;

	writecmd_oleddispl(&cmd, 1);
}

// write byte
void lcd_write_byte(u08 data)
{
	writedata_oleddispl(data);
}

void lcd_write_dram(unsigned char *source)
{
	short pa, col;
	for(pa = 0; pa < LCD_ROWS; pa++)
	{
		lcd_set_pos(pa, 0);
		for(col = 0; col < LCD_COLS; col++)
			lcd_write_byte(source[(pa * LCD_COLS) + col]);
	}
}
#ifdef SSD1325
void lcd_set_pos(u08 row, u08 col)
{
	unsigned char cmd[5];
	cmd[0] = 0x15;
	cmd[1] = col / 2;
	cmd[2] = 63;
	writecmd_oleddispl(cmd, 3);

	cmd[0] = 0x75;
	cmd[1] = row;
	cmd[2] = 79;
	writecmd_oleddispl(cmd, 3);

}

void lcd_set_arrpos(u08 w, u08 h, u08 row, u08 col)
{
	unsigned char cmd[3];
	cmd[0] = 0x15;
	cmd[1] = col / 2;
	cmd[2] = col / 2 + w / 2 - 1;
	writecmd_oleddispl(cmd, 3);

	cmd[0] = 0x75;
	cmd[1] = row;
	cmd[2] = row + h - 1;
	writecmd_oleddispl(cmd, 3);

}

void lcd_clear(void)
{
	short rowaddr, coladdr;
	for(rowaddr = 0; rowaddr < LCD_ROWS; rowaddr++)
	{
		lcd_set_pos(rowaddr, 0);
		for(coladdr = 0; coladdr < LCD_COLS / 2; coladdr++)
		lcd_write_byte(0);
	}
}

void show_icon(const unsigned char *bitmap, u08 w, u08 h, u08 x, u08 y, u08 mode)
{
	u08 pix; //off, tx, ty, pix, hb;
//	unsigned short col;
	unsigned short len;

	x = 2 * ((x + 1) >> 1);
	w = 2 * ((w + 1) >> 1);
	bitmap_ptr_dbg = bitmap;
	len_dbg = len = (w * h) / 2;
	lcd_set_arrpos(w, h, y, x);
	for(position = 0; len_dbg > 0; len_dbg--, position++)
	{
		if(((position % 8 == 0) && (position != 0)) || (position + 1 > w))
		{
			if(position + 1 > w)
			position = 0;
			bitmap_ptr_dbg++;
		}
		pix = 0;
		if((((*bitmap_ptr_dbg) >> (position) % 8) & 0x01) == 1)
		pix = 0x0F;

		position++;

		if(position + 1 > w)
		;
		else
		{
			if((((*bitmap_ptr_dbg) >> (position) % 8) & 0x01) == 1)
			pix |= 0xF0;
		}

		switch(mode)
		{
			case LCD_PIXEL_OFF:
			pix = ~pix;
			break;
			default:
			case LCD_PIXEL_ON:
			break;
		}
		lcd_write_byte(pix);
	}
#if 0
	off = y & 7;
	hb = (h+7) / 8;
	for ( ty = 0; ty < hb; ty++ )
	{
		lcd_set_pos( ty + y / 8, x );
		for ( tx = 0; tx < w; tx++ )
		{
			if( off )
			{
				col = *(bitmap+ ty + tx * hb) << 8;
				if( ty )
				col |=*( bitmap + (ty - 1) + tx * hb );
				col >>= 8 - off;
				pix = col;
			}
			else
			pix = *( bitmap + ty + tx * hb );

			switch( mode )

			{

				case LCD_PIXEL_OFF:

				pix = ~pix;

				break;

				default:
				case LCD_PIXEL_ON:

				break;

			}

			lcd_write_byte( pix );

		}

	}
#endif
}

u08 draw_point(u08 x, u08 y, u08 v)

{
#if 0
	u08 ty = 0, tval, bit;

	if( y >= ( LCD_ROWS * 8 ) )

	return -1;

	if( x >= LCD_COLS )

	return -1;

	if( y )

	ty = ( y / 8 );

	bit = y - ( ty * 8 );

	// set dram pointer

	lcd_set_pos( ty, x );

	tval = ( 1 << bit );
	if ( v == LCD_PIXEL_OFF )
	tval = ~tval;
	// set dram pointer

	lcd_set_pos( ty, x );
	lcd_write_byte( tval );

#endif
	return 0;

}

u08 draw_vertical_line(u08 x, u08 y, u08 l, u08 pattern)

{
	unsigned char cmd[6];

	if(y >= (LCD_ROWS))
	return -1;
	if(x + l >= LCD_COLS)
	return -1;

	cmd[0] = 0x24;
	cmd[1] = x / 2;
	cmd[2] = y;
	cmd[3] = (x + l) / 2;
	cmd[4] = y;
	cmd[5] = pattern;
	writecmd_oleddispl(cmd, 6);
	return 0;

}

/****************************
 *	draw_line
 *	args: x1=StartCol, y1=StartRow, x2=EndCol, y1=EndRow
 *		  state=LCD_PIXEL_OFF/LCD_PIXEL_ON/LCD_PIXEL_INV
 *********************************************************/

u08 draw_line(u08 x1, u08 y1, u08 x2, u08 y2, u08 pattern)
{
	short dx, dy, sdx, sdy, px, py, dxabs, dyabs, i;
	float slope;

	unsigned char cmd[6];

	if(y2 >= (LCD_ROWS))
	return -1;
	if(x2 >= LCD_COLS)
	return -1;

	cmd[0] = 0x24;
	cmd[1] = x1 / 2;
	cmd[2] = y1;
	cmd[3] = x2 / 2;
	cmd[4] = y2;
	cmd[5] = pattern;
	writecmd_oleddispl(cmd, 6);
	return 0;
}

u08 fill_rect(u08 left, u08 top, u08 right, u08 bottom, u08 pattern)
{
	u08 x, y, l, tval;
	unsigned char cmd[6];

	if(bottom >= (LCD_ROWS))
	return -1;
	if(right >= LCD_COLS)
	return -1;

	cmd[0] = 0x24;
	cmd[1] = left / 2;
	cmd[2] = top;
	cmd[3] = right / 2;
	cmd[4] = bottom;
	cmd[5] = pattern;
	writecmd_oleddispl(cmd, 6);
	return 0;
}

void draw_rectangle(u08 left, u08 top, u08 right, u08 bottom, u08 linestate, u08 fillstate)
{
//	draw_line( left, top, right, top, linestate );
	draw_vertical_line(left, top, left + right, linestate);
	draw_line(left, top, left, bottom, linestate);
	draw_line(right, top, right, bottom, linestate);
	draw_line(left, bottom, right, bottom, linestate);
	fill_rect(left, top, right, bottom, fillstate);

}

FONT_INFO *font;
u08 hfont; //, charsize;

void select_font(FONT_INFO *f)
{
	font = f;
	hfont = f->heightPages;
}
void draw_text(u08 x, u08 y, unsigned char *text, u08 m, u08 l)
{
	u08 i, tx, ty;
	unsigned char *p, ch;
	FONT_CHAR_INFO *ch_inf;

	ty = y - hfont;
	x = ((x + 1) >> 1) * 2;
	tx = x;
	for(p = text, i = 0; *p; i++, p++)
	{

		if(*p >= 0xE0)
		ch = *p - (font->startChar + 0x61);
		else
		ch = *p - font->startChar;
//		ch_inf = &(font->charInfo[ch]);
		ch_inf = (FONT_CHAR_INFO *)font->charInfo;
		ch_inf = ch_inf + ch;
		if(tx + ch_inf->widthBits > LCD_COLS)
		{
			i = 0;
			tx = x;
			ty += hfont;
		}
		tx = ((tx + 1) >> 1) * 2;
		show_icon(font->data + ch_inf->offset, ch_inf->widthBits, hfont, tx, ty, m);
		tx += ch_inf->widthBits;

		/*Print space*/
		if(tx + 2 > LCD_COLS)
		{
			i = 0;
			tx = x;
			ty += hfont;
		}

		show_icon(font->data, 1, hfont, tx, ty, m);
		tx += 2;
		tx = (tx >> 1) * 2;
	}

	if(l == 0 || l <= tx)
	return;
	l -= tx;
	while(l > 0)
	{
		if(tx + 1 > LCD_COLS)
		{
			i = 0;
			tx = x;
			ty += hfont;
		}
		show_icon(font->data, 1, hfont, tx, ty, m);
		tx += 1;
		l--;
	}
}
#endif

#ifdef SSD1305
void lcd_set_pos(u08 row, u08 col)
{
	lcd_send_cmd(LCD_CMD_SPAGE, row);
	lcd_send_cmd(LCD_CMD_COL, (col >> 4) & 0x0F);
	lcd_send_cmd(0x00, col & 0x0F);
}

void lcd_clear(void)
{
	short pa, col;
	for(pa = 0; pa < LCD_ROWS; pa++)
	{
		lcd_set_pos(pa, 0);
		for(col = 0; col < LCD_COLS; col++)
		{
			lcd_write_byte(0x00);
		}
	}
}

void show_icon(const unsigned char *bitmap, u08 w, u08 h, u08 x, u08 y, u08 mode)
{
	u08 off, tx, ty, pix, hb;
	unsigned short col;

	off = y & 7;
	hb = (h + 7) / 8;
	for(ty = 0; ty < hb; ty++)
	{
		lcd_set_pos(ty + y / 8, x);
		for(tx = 0; tx < w; tx++)
		{
			if(off)
			{
				col = *(bitmap + ty + tx * hb) << 8;
				if(ty)
				col |= *(bitmap + (ty - 1) + tx * hb);
				col >>= 8 - off;
				pix = col;
			}
			else
			pix = *(bitmap + ty + tx * hb);

			switch(mode)

			{

				case LCD_PIXEL_OFF:

				pix = ~pix;

				break;

				default:
				case LCD_PIXEL_ON:

				break;

			}

			lcd_write_byte(pix);

		}

	}

}

u08 draw_point(u08 x, u08 y, u08 v)

{
	u08 ty = 0, tval, bit;

	if(y >= (LCD_ROWS * 8))

	return -1;

	if(x >= LCD_COLS)

	return -1;

	if(y)

	ty = (y / 8);

	bit = y - (ty * 8);

	// set dram pointer

	lcd_set_pos(ty, x);

	tval = (1 << bit);
	if(v == LCD_PIXEL_OFF)
	tval = ~tval;
	// set dram pointer

	lcd_set_pos(ty, x);
	lcd_write_byte(tval);

	return 0;

}

u08 draw_vertical_line(u08 x, u08 y, u08 l, u08 v)

{
	u08 ty = 0, tval = 0, bit, idx;

#if 0
	if(y >= ( LCD_ROWS * 8 ) )
	return -1;

	if(x >= LCD_COLS )
	return -1;

	if( y )
	ty = ( y / 8 );

	bit = y - ( ty * 8 );
	// set dram pointer
	lcd_set_pos( ty, x );

	while(l>0)
	tval |= (1 << --l);
	tval = ( tval << bit );
	if ( v == LCD_PIXEL_OFF )
	tval = ~tval;
	// set dram pointer
	lcd_set_pos( ty, x );
	lcd_write_byte( tval );

#endif
	if(y >= (LCD_ROWS * 8))
	return -1;
	if(x >= LCD_COLS)
	return -1;

	if(y)
	ty = (y / 8);
	bit = y - (ty * 8);
//	idx = 8 - bit;

	while(1)
	{
//		// set dram pointer
		lcd_set_pos(ty, x);
		ty++;

		tval = idx = 0;
		while(idx < (8 - bit))
		{
			tval |= (1 << idx++);
			l--;
			if(l == 0)
			break;
		}
		tval = (tval << bit);
		if(v == LCD_PIXEL_OFF)
		tval = ~tval;
		// set dram pointer

//		lcd_set_pos( ty, x );
		lcd_write_byte(tval);
		if(l == 0)
		break;
		bit = 0;

	}
	return 0;

}

/****************************
 *	draw_line
 *	args: x1=StartCol, y1=StartRow, x2=EndCol, y1=EndRow
 *		  state=LCD_PIXEL_OFF/LCD_PIXEL_ON/LCD_PIXEL_INV
 *********************************************************/

void draw_line(u08 x1, u08 y1, u08 x2, u08 y2, u08 v)
{
	short dx, dy, sdx, sdy, px, py, dxabs, dyabs, i;
	float slope;

	dx = x2 - x1;
	dy = y2 - y1;
	dxabs = sdx = 0;
	if(dx > 0)
	{
		dxabs = dx;
		sdx = 1;
	}
	else if(dx < 0)
	{
		dxabs = -dx;
		sdx = -1;
	}
	dyabs = sdy = 0;
	if(dy > 0)
	{
		dyabs = dy;
		sdy = 1;
	}
	else if(dy < 0)
	{
		dyabs = -dy;
		sdy = -1;
	}
	if(dxabs >= dyabs) /* the line is more horizontal than vertical */
	{
		slope = (float)dy / (float)dx;
		for(i = 0; i != dx; i += sdx)
		{
			px = i + x1;
			py = slope * i + y1;
			draw_point(px, py, v);
		}
	}
	else /* the line is more vertical than horizontal */
	{
		slope = (float)dx / (float)dy;
		for(i = 0; i != dy; i += sdy)
		{
			px = slope * i + x1;
			py = i + y1;
			draw_point(px, py, v);
		}
	}
}

void fill_rect(u08 left, u08 top, u08 right, u08 bottom, u08 v)
{
	u08 x, y, l;
	for(x = left; x <= right; x++)
	{
//		for ( y = top + 1; y < bottom; y++ )
		y = top;
		l = bottom - top + 1;
		draw_vertical_line(x, y, l, v);

//			draw_point( x, y, v );
	}
}
void draw_rectangle(u08 left, u08 top, u08 right, u08 bottom, u08 linestate, u08 fillstate)
{
	draw_line(left, top, right, top, linestate);
	draw_line(left, top, left, bottom, linestate);
	draw_line(right, top, right, bottom, linestate);
	draw_line(left, bottom, right, bottom, linestate);
	fill_rect(left, top, right, bottom, fillstate);

}

const FONT_INFO *font;
u08 hfont; //, charsize;

void select_font(const FONT_INFO *f)
{
	font = f;
	hfont = f->heightPages;
}
void draw_text(u08 x, u08 y, char *text, u08 m, u08 l)
{
#if 1
	u08 i, tx, ty;
	char *p, ch;
	FONT_CHAR_INFO *ch_inf;

	ty = y - hfont;
	x = ((x + 1) >> 1) * 2;
	tx = x;
	for(p = text, i = 0; *p; i++, p++)
	{

		if(*p >= 0xE0)
		ch = *p - (font->startChar + 0x61);
		else
		ch = *p - font->startChar;
//		ch_inf = &(font->charInfo[ch]);
		ch_inf = (FONT_CHAR_INFO *)font->charInfo;
		ch_inf = ch_inf + ch;
		if(tx + ch_inf->widthBits > LCD_COLS)
		{
			i = 0;
			tx = x;
			ty += hfont;
		}
		tx = ((tx + 1) >> 1) * 2;
		show_icon(font->data + ch_inf->offset, ch_inf->widthBits, hfont, tx, ty, m);
		tx += ch_inf->widthBits;

		/*Print space*/
		if(tx + 2 > LCD_COLS)
		{
			i = 0;
			tx = x;
			ty += hfont;
		}

		show_icon(font->data, 1, hfont, tx, ty, m);
		tx += 2;
		tx = (tx >> 1) * 2;
	}

	if(l == 0 || l <= tx)
	return;
	l -= tx;
	while(l > 0)
	{
		if(tx + 1 > LCD_COLS)
		{
			i = 0;
			tx = x;
			ty += hfont;
		}
		show_icon(font->data, 1, hfont, tx, ty, m);
		tx += 1;
		l--;
	}

#else
	u08 i, tx, ty;
	unsigned char *p, ch;
	FONT_CHAR_INFO *ch_inf;

	ty = y - hfont;
	tx = x;
	for(p = text, i = 0; *p; i++, p++)
	{

		if(*p >= 0xE0)
		ch = *p - (font->startChar + 0x61);
		else
		ch = *p - font->startChar;
//		ch_inf = &(font->charInfo[ch]);
		ch_inf = (FONT_CHAR_INFO *)font->charInfo;
		ch_inf = ch_inf + ch;
		if(tx + ch_inf->widthBits > LCD_COLS)
		{
			i = 0;
			tx = x;
			ty += hfont;
		}

		show_icon(font->data + ch_inf->offset, ch_inf->widthBits, hfont, tx, ty, m);
		tx += ch_inf->widthBits;

		/*Print space*/
		if(tx + 1 > LCD_COLS)
		{
			i = 0;
			tx = x;
			ty += hfont;
		}

		show_icon(font->data, 1, hfont, tx, ty, m);
		tx += 1;
	}

	if(l == 0 || l <= tx)
	return;
	l -= tx;
	while(l > 0)
	{
		if(tx + 1 > LCD_COLS)
		{
			i = 0;
			tx = x;
			ty += hfont;
		}
		show_icon(font->data, 1, hfont, tx, ty, m);
		tx += 1;
		l--;
	}
#endif
}

#endif

