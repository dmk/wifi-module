/*
 * OpenHub_node_OLED_gui.cpp
 *
 *  Created on: Oct 27, 2016
 *      Author: admin
 */

#if USE_OLED
#include <OLEDDisplay.h>
#include <OLEDDisplayFonts.h>
#include <OLEDDisplayUi.h>

void drawProgress(OLEDDisplay *display, int percentage, String label)
{
	display->clear();
	display->setTextAlignment(TEXT_ALIGN_CENTER);
	display->setFont(ArialMT_Plain_10);
	display->drawString(64, 10, label);
	display->drawProgressBar(2, 28, 124, 10, percentage);
	display->display();
}
#endif



