/*
 * LbotConfig_WebInterface.h
 *
 *  Created on: Mar 6, 2018
 *      Author: dkunin
 */

#ifndef LBOTCONFIG_WEBINTERFACE_H_
#define LBOTCONFIG_WEBINTERFACE_H_


void loopWeb();
void setupWeb();
boolean IsWebStarted();

#endif /* LBOTCONFIG_WEBINTERFACE_H_ */
