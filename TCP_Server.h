/*
 * TCP_Server.h
 *
 *  Created on: Apr 12, 2018
 *      Author: dkunin
 */

#ifndef TCP_SERVER_H_
#define TCP_SERVER_H_


void startTcpServer(void);
void loop_tcp_server(void);



#endif /* TCP_SERVER_H_ */
