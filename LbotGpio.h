/*
 * LbotGpio.h
 *
 *  Created on: May 19, 2018
 *      Author: admin
 */

#ifndef LBOTGPIO_H_
#define LBOTGPIO_H_
#include "Arduino.h"
#include "types.h"

#define INPUT_PORT0	0
#define INPUT_PORT1	1

class LbotGpio
{
public:
	LbotGpio();
	void begin(void);
	byte getDI0();
	byte getDI1();
	void setDO0();
	void setDO1();
	void clrDO0();
	void clrDO1();
	byte pollDI(byte &port_val);
};

#endif /* LBOTGPIO_H_ */
