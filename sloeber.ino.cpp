#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2021-07-21 19:29:26

#include "Arduino.h"
#include "Arduino.h"
#include <stdio.h>
#include "board_conf.h"
#include "fw_ver.h"
#include "font.h"
#include "types.h"
#include "IoT_HostInteface.h"
#include "LbotConfig_WebInterface.h"
#include "TCP_Server.h"
#include "LbotConfig.h"
#include "LbotSensorReading.h"
#include "spidev.h"
#include "str_tools.h"
#include "LbotGpio.h"
#include "crc16.h"
#include "debugMacros.h"
#include "hostVL667.h"
#include "FS.h"
#define my_draw_text(...)
#define my_lcd_clear()
#define my_show_icon(...)
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>

void setup() ;
void loop() ;
void gpioLoop(long now) ;
boolean checkWifiConnection(long now);
void tcpConnection(long now);
void pubSubConnection(long now);
void setupOled () ;

#include "WiFi_VL667_2_6_3_core.ino"


#endif
