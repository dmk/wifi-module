#ifndef GLCD_H
#define GLCD_H

/***********************************************
 *             glcd.h
 *
 *
 * Setup and external funct declares for glcd.c
 *
 * Please Select/alter settings to match your setup
 *
 * NOTE: Parallel mode only currently supported
 * with this code.
 ***********************************************/
typedef unsigned char u08;
/* Include Bitmap header */
#include "board_conf.h"
#include "font.h"

/*LCD 128x64 dots - E.G. Displaytech 64128G*/
#define LCD_ROWS		64           // 1 bit row x 64 = 64 dots
#define LCD_COLS		128         // 1 bit colum x 128 = 128 Dots

#define LCD_TXTLINE1		16
#define LCD_TXTLINE2		39
#define LCD_TXTLINE23		48
#define LCD_TXTLINE3		63
#define LCD_IMGLINE1		0
#define LCD_IMGLINE12		16
#define LCD_IMGLINE2		24
#define LCD_IMGLINE23		32
#define LCD_IMGLINE3		48

/* Functions */

extern void lcd_read_dram(unsigned char *dest);

extern void lcd_write_byte(u08 data);

extern void lcd_write_dram(unsigned char *source);

extern void lcd_set_pos(u08 row, u08 col);

extern void lcd_clear(void);

extern void show_icon(const unsigned char *bitmap, u08 w, u08 h, u08 x, u08 y, u08 mode);
#ifdef SSD1305
void draw_line(u08 x1, u08 y1, u08 x2, u08 y2, u08 v);
void fill_rect(u08 left, u08 top, u08 right, u08 bottom, u08 v);
#endif
#ifdef SSD1325
u08 draw_line(u08 x1, u08 y1, u08 x2, u08 y2, u08 v);
u08 fill_rect(u08 left, u08 top, u08 right, u08 bottom, u08 v);
#endif
u08 draw_point(u08 x, u08 y, u08 v);

extern void draw_rectangle(u08 left, u08 top, u08 right, u08 bottom, u08 linestate, u08 fillstate);

extern void select_font(const FONT_INFO *f);

extern void draw_text(u08 x, u08 y, char *text, u08 m, u08 l);

#define LCD_DIR_READ  0xFF

#define LCD_DIR_WRITE 0x00

#define LCD_DELAY 		1

/* Display ON/OFF	001010111O
 * O 0=OFF 1=ON
 */

#define LCD_CMD_ON		0xAE

/* Initial display line ST5-0
 * Specify DDRAM line for COM1
 */

#define LCD_CMD_IDL		0x40

/* Set reference voltage mode 0010000001
 */

#define LCD_CMD_SRV		0x81

/* Set page address	001011DCBA
 * D-A P3-P1
 */

#define LCD_CMD_SPAGE		0xB0

/* Set column address 000001DCBA
 * D-A  Y7-4
 * next Y3-0			
 */

#define LCD_CMD_COL		0x10

/* ADC select 001010011A
 * A 0=normal direction 1=reverse horizontal
 */

#define LCD_CMD_ADC		0xA0

/* SHL select 001100SXXX
 * S 0=normal 1=reverse vertical
 */

#define LCD_CMD_SHL		0xC0

/* Reverse display ON/OFF 001010011R
 * R 0=normal 1=reverse
 */

#define	LCD_CMD_REVERSE		0xA6

/* Entire display ON/OFF 001010010E
 * E 0=normal 1=entire
 */

#define LCD_CMD_EON		0xA4

/* LCD bias select 001010001B
 * B Bias  
 */

#define LCD_CMD_BIAS		0xA2

/* Set modify-read 0011100000
 */

#define LCD_CMD_SMR		0xE0

/* Reset modify-read 0011101110
 */

#define LCD_CMD_RMR		0xEE

/* Reset 0011100010
 * Initialize the internal functions
 */

#define LCD_CMD_RESET		0xE2

/* Power control 0000101CRF
 * control power circuite operation
 */

#define LCD_CMD_POWERC		0x28

/* Regulator resistor select 0000100CBA
 * select internal resistor ration
 */

#define LCD_CMD_RES		0x20	// Regulator resistor select
/* Set static indicator 001010110S * S 0=off 1=on * next 00XXXXXXB C */
#define LCD_CMD_SIR		0xAC

/* Read Status 01BAOR0000
 * B BUSY
 * A ADC
 * O ON/OFF
 * R RESETB
 */

#define LCD_STAT_BUSY	0x80

#define LCD_STAT_ADC	0x40

#define LCD_STAT_ON		0x20

#define LCD_STAT_RESETB	0x10

#define LCD_POWERC_VC	0x04

#define LCD_POWERC_VR	0x02

#define LCD_POWERC_VF	0x01

#define LCD_PIXEL_OFF   0

#define LCD_PIXEL_ON    1

#define LCD_PIXEL_INV   LCD_PIXEL_ON
#define LCD_PATTERN_LIGHT   0xff

//#define LCD_PATTERN_DARK	0x33
#define LCD_PATTERN_DARK	0xff

#endif


