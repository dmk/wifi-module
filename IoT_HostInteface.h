/*
 * HostInteface.h
 *
 *  Created on: Mar 5, 2018
 *      Author: dkunin
 */

#ifndef IOT_HOSTINTEFACE_H_
#define IOT_HOSTINTEFACE_H_
#include "board_conf.h"

typedef enum
{
	HUM_TYPE,
	TEMP_HUM_TYPE,
	TEMP_TYPE,
	PRESS_TYPE,
	TEMP_PRESS_TYPE,
	RELAY_TYPE,
	DIGINPUT0_TYPE,
	DIGINPUT1_TYPE
}sensor_data_t;


void sendDataToHost(sensor_data_t type, double value);

#if (HOSTING  == PUBSUB)
void handlePubSub(long now);
#endif

#endif /* IOT_HOSTINTEFACE_H_ */
