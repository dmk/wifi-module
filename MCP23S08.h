#ifndef _MCP23S08_H
#define _MCP23S08_H

#ifdef MCP23S08_PUB
#define MCP23S08PUB
#else
#define MCP23S08PUB extern
#endif

/**************************************************************/
#define IODIR   	0x00
#define IPOL   	  0x01
#define GPINTEN   0x02
#define DEFVAL    0x03
#define INTCON 		0x04
#define IOCON		  0x05
#define GPPU  		0x06
#define INTF 		  0x07
#define INTCAP 		0x08
#define GPIO  		0x09
#define OLAT   		0x0A

#define CONTROL_BYTE 0x40
#define ADDR_PINS 0x00
#define WR_CMD  0
#define RD_CMD   1

#define GP7	0x80
#define GP6	0x40
#define GP5	0x20
#define GP4	0x10
#define GP3	0x08
#define GP2	0x04
#define GP1	0x02
#define GP0	0x01

#define GPI7	0x80
#define GPI6	0x40
#define GPI5	0x20
#define GPI4	0x10
#define GPI3	0x08
#define GPI2	0x04
#define GPI1	0x02
#define GPI0	0x01

#define GPO7	0x0
#define GPO6	0x0
#define GPO5	0x0
#define GPO4	0x0
#define GPO3	0x0
#define GPO2	0x0
#define GPO1	0x0
#define GPO0	0x0

#define GPPU7	0x80
#define GPPU6	0x40
#define GPPU5	0x20
#define GPPU4	0x10
#define GPPU3	0x08
#define GPPU2	0x04
#define GPPU1	0x02
#define GPPU0	0x01


/*MCP23S08PUB*/ void MCP23S08Init(unsigned char dir, unsigned char pulup, unsigned char port);
MCP23S08PUB unsigned char MCP23S08Read(unsigned char reg);
MCP23S08PUB void MCP23S08Write(unsigned char reg, unsigned char data);
void MCP23S08SetGPIObits(unsigned char port_data, unsigned char mask);
void MCP23S08SetDIR(unsigned char dir, unsigned char mask);


//MCP23S08PUB BYTE MCP23S08WriteWord(WORD data, DWORD address);
//MCP23S08PUB WORD MCP23S08ReadWord(DWORD address);
//MCP23S08PUB BYTE MCP23S08WriteArray(DWORD address, BYTE *pData, WORD nCount);
//MCP23S08PUB void MCP23S08ReadArray(DWORD address, BYTE *pData, WORD nCount);
//MCP23S08PUB void MCP23S08WaitProgram(void);
//MCP23S08PUB void MCP23S08ChipErase(void);
//MCP23S08PUB void MCP23S08SectorErase(DWORD address);
//MCP23S08PUB WORD MCP23S08CheckID();

#endif //_MCP23S08_H

