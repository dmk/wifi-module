/*OLED SPI driver*/
#define OLED_PUB

#include "Arduino.h"
#include "spidev.h"
#include "MCP23S08.h"
#include "oledSSD1305.h"

void oleddispl_on(unsigned char mode)
{
	unsigned char cmd = 0xA4;
	if(mode == 1)
		cmd = 0xA5;
	writecmd_oleddispl(&cmd, 1);

}

void oled_reset(unsigned char val)
{
	unsigned char port_stat;

	port_stat = MCP23S08Read(OLAT);
	if(val == 0)
	{
		port_stat &= ~(1 << OLED_RESET_BIT);
	}
	else
	{
		port_stat |= 1 << OLED_RESET_BIT;
	}
	MCP23S08Write(OLAT, port_stat);

}
void oled_power(unsigned char val)
{
	unsigned char port_stat;

	port_stat = MCP23S08Read(OLAT);
	if(val == 0)
	{
		port_stat &= ~(1 << OLED_POWER_BIT);
	}
	else
	{
		port_stat |= 1 << OLED_POWER_BIT;
	}
	MCP23S08Write(OLAT, port_stat);

}
void oled_dc(unsigned char val)
{
	unsigned char port_stat;

	port_stat = MCP23S08Read(OLAT);
	if(val == 0)
	{
		port_stat &= ~(1 << OLED_DC_BIT);
	}
	else
	{
		port_stat |= 1 << OLED_DC_BIT;
	}
	MCP23S08Write(OLAT, port_stat);

}

#ifdef SSD1305

/************************************************
 * Initialize OLED display
 *
 * a) Start SPI
 * b) Do Power up Sequence
 *	 1. Send Display Off Command
 *	 2. Initialisation
 *	 3. Clear Screen
 *	 4. Power up Vcc (12V)
 *	 5. wait 100 ms
 *	 6. Send Display On Command
 ************************************************/
void init_oleddispl(void)
{
	unsigned char cmd[5];
	int i;

	oled_reset(0);
	delayMicroseconds(10);         // hang out for 10 microseconds
	oled_reset(1);

	cmd[0] = 0xAE;
	writecmd_oleddispl(cmd,1);
	cmd[0] = 0xD5;
	cmd[1] = 0xF0;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xA8;
	cmd[1] = 0x3F;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xD3;
	cmd[1] = 0x00;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0x40;
	writecmd_oleddispl(cmd,1);
	cmd[0] = 0xA1;
	writecmd_oleddispl(cmd,1);
	cmd[0] = 0xC8;
	writecmd_oleddispl(cmd,1);
	cmd[0] = 0xAD;
	cmd[1] = 0x8E;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xD8;
	cmd[1] = 0x05;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0x91;
	cmd[1] = 0x3F;
	cmd[2] = 0x3F;
	cmd[3] = 0x3F;
	cmd[4] = 0x3F;
	writecmd_oleddispl(cmd,5);
	cmd[0] = 0x81;
	cmd[1] = 0x8F;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xD9;
	cmd[1] = 0x11;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xDA;
	cmd[1] = 0x12;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xDB;
	cmd[1] = 0x34;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xA4;
	writecmd_oleddispl(cmd,1);
	cmd[0] = 0xA6;
//		cmd[0] = 0xA7;
	writecmd_oleddispl(cmd,1);

	oled_power(1);
	delayMicroseconds(100);// hang out for 100 microseconds
//	_delay_us(100000);
	cmd[0] = 0xAF;
	writecmd_oleddispl(cmd,1);

//		cmd[0] = 0xA5;
//		writecmd_oleddispl(cmd,1);

}
#elif defined(SSD1325)
/************************************************
 * Initialize OLED display
 *
 * a) Start SPI
 * b) Do Power up Sequence
 *	 1. Send Display Off Command
 *	 2. Initialisation
 *	 3. Clear Screen
 *	 4. Power up Vcc (12V)
 *	 5. wait 100 ms
 *	 6. Send Display On Command
 ************************************************/
void init_oleddispl(void)
{
	unsigned char cmd[5];
	int i;

	OLED_RESET_PIN = 0;
	_delay_us(100);
	OLED_RESET_PIN = 1;

	cmd[0] = 0xAE;           // Set Display OFF
	writecmd_oleddispl(cmd,1);
	cmd[0] = 0xB3;//Set Display Clock Divide
	cmd[1] = 0x91;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xA8;//Set Multiplex ratio
	cmd[1] = 0x3F;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xA2;//Set Display offset
	cmd[1] = 0x4C;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xA1;//Set display Start line
	cmd[1] = 0x00;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xAD;//Set master configuration
	cmd[1] = 0x02;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xA0;//Set Re-Map
	cmd[1] = 0x50;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0x86;//Set Current Range
	writecmd_oleddispl(cmd,1);
	cmd[0] = 0x81;//Set Contrast Control
	cmd[1] = 0x7F;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xB2;//Set Row period
	cmd[1] = 0x51;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xB1;//Set Phase Length
	cmd[1] = 0x55;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xBC;//Set Precharge Voltage
	cmd[1] = 0x10;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xB4;//Set Precharge Compensation level
	cmd[1] = 0x02;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xB0;//Set Precharge Compensation enable
	cmd[1] = 0x28;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xBE;//Set VCOMH
	cmd[1] = 0x1C;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xBF;//Set VSL
	cmd[1] = 0x0D;
	writecmd_oleddispl(cmd,2);
	cmd[0] = 0xA4;//Set display mode
	writecmd_oleddispl(cmd,1);
	OLED_POWER_PIN = 1;
	_delay_us(100000);
	cmd[0] = 0xAF;
	writecmd_oleddispl(cmd,1);

}
#endif

/************************************************
 * Close OLED display
 *
 * a) Stop SPI
 * b) Do Power down Sequence
 *	 1. Send display off Command
 *	 2. Power down Vcc (12V)
 *	 3. wait 100 ms
 *	 4. Ready for Power down Vdd
 ************************************************/
void close_oleddispl(void)
{
	unsigned char cmd = 0xAE;

	writecmd_oleddispl(&cmd, 1);
	delayMicroseconds(100);
	oled_power(0);

}

void writecmd_oleddispl(unsigned char *cmd, int len)
{
//	GLOB_CRITSECT_START;
	oled_dc (OLED_CMD);
	selectSpiDev (OLED_CS);
	delayMicroseconds(1);
	while(len--)
	{
//		GLOB_CRITSECT_START;

		transferSpiDev(*cmd++, 0x11);

//		WriteSPI2(*cmd++);
//		while(!SPI2STATbits.SPIRBF)
//			;
//		ReadSPI2();
//		GLOB_CRITSECT_STOP;
	}
	delayMicroseconds(1);
	selectSpiDev (NONE_CS);
}
void writedata_oleddispl(unsigned char data)
{
//	GLOB_CRITSECT_START;
	oled_dc (OLED_DATA);
	selectSpiDev (OLED_CS);
	delayMicroseconds(1);
//	GLOB_CRITSECT_START;

	transferSpiDev(data, 0x11);

//	WriteSPI2(data);
//	while(!SPI2STATbits.SPIRBF)
//		;
//	ReadSPI2();
//	GLOB_CRITSECT_STOP;

	delayMicroseconds(1);
	selectSpiDev (NONE_CS);
}

